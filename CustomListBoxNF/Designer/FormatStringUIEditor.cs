﻿using CustomListBoxNF.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace CustomListBoxNF.Designer
{
  #region define class Editor for property FormatString
  internal class FormatStringEditor : UITypeEditor
  {
    public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
    {
      return UITypeEditorEditStyle.Modal;
    }
    public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
    {
      if (provider != null)
      {
        IWindowsFormsEditorService editorService = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;
        if (editorService != null)
        {
          using (FormatStringDialog formatStringDialog = new FormatStringDialog())
          {
            DialogResult result = editorService.ShowDialog(formatStringDialog);
            if (result == DialogResult.OK)
            {
              return formatStringDialog.ValueSelected;
            }
          }
          return value;
        }
      }
      return base.EditValue(context, provider, value);

    }
    public override bool GetPaintValueSupported(ITypeDescriptorContext context)
    {
      return false;
    }
  }
  #endregion
}
