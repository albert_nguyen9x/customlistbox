﻿using CustomListBoxNF.CustomControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace CustomListBoxNF.Designer
{
  #region define class designer for custom list box
  internal class CustomListBoxDesigner : ControlDesigner
  {
    public override void Initialize(IComponent component)
    {
      base.Initialize(component);
      IDesignerHost designer = (IDesignerHost)GetService(typeof(IDesignerHost));
      if (designer != null)
      {
        INameCreationService nameCreation = designer.GetService(typeof(INameCreationService)) as INameCreationService;
        CustomListBoxNameCreation customListBoxNameCreation = new CustomListBoxNameCreation(nameCreation);
        designer.RemoveService(typeof(INameCreationService));
        designer.AddService(typeof(INameCreationService), customListBoxNameCreation);
      }
    }
    private readonly string[] _propertiesToRemove =
        {
            "BackgroundImage", "BackgroundImageLayout",
            "RightToLeft","ImeMode","Text"
        };

    private DesignerActionListCollection lists;

    public override DesignerActionListCollection ActionLists
    {
      get
      {
        if (lists == null)
        {

          lists = new DesignerActionListCollection();
          lists.Add(new CustomListBoxActionList(this));
        }
        return lists;
      }
    }

    protected override void PostFilterAttributes(IDictionary attributes)
    {
      foreach (var property in _propertiesToRemove)
      {
        attributes.Remove(property);
      }
      base.PostFilterAttributes(attributes);

    }
    protected override void PostFilterEvents(IDictionary events)
    {
      events.Remove("Paint");
      events.Remove("BackgroundImageChanged");
      events.Remove("BackgroundImageLayoutChanged");
      base.PostFilterEvents(events);
    }
  }
  #endregion
}


