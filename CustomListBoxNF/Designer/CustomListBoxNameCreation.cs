﻿using CustomListBoxNF.CustomControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Designer
{
  public class CustomListBoxNameCreation : INameCreationService
  {
    private readonly string _defalutName = "listBox";
    INameCreationService _nameCreation;
    int counter = 1;

    public CustomListBoxNameCreation(INameCreationService nc)
    {
      this._nameCreation = nc;
    }
    public string CreateName(IContainer container, Type dataType)
    {
      if (dataType != typeof(CustomListBox))
      {
        return _nameCreation.CreateName(container, dataType);
      }
      string name = "";
      do
      {
        counter++;
        name = _defalutName + counter;
      } while (!IsUnique(name, container));
      return name;
    }

    private bool IsUnique(string name, IContainer container)
    {
      for (int i = 0; i < container.Components.Count; i++)
      {
        // Check component name for match with unique ID string.
        if (container.Components[i].Site.Name.Equals(name))
        {
          return false;
        }
      }
      return true;
    }

    public bool IsValidName(string name)
    {
      return _nameCreation.IsValidName(name);
    }
    public void ValidateName(string name)
    {
      _nameCreation.ValidateName(name);
    }
  }
}
