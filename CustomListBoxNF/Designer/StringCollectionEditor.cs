﻿using CustomListBoxNF.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomListBoxNF.Designer
{
  #region clone StringCollection Editor fix error refresh Items when add in design mode
  internal class StringCollectionEditor : CollectionEditor
  {

    public StringCollectionEditor(Type type) : base(type)
    {
    }
    protected override CollectionForm CreateCollectionForm()
    {
      return new StringCollectionForm(this);
    }
    protected override string HelpTopic
    {
      get
      {
        return "net.ComponentModel.StringCollectionEditor";
      }
    }
    protected override object SetItems(object editValue, object[] value)
    {
      if (editValue != null)
      {
        Array oldValue = (Array)GetItems(editValue);
        bool valueSame = (oldValue.Length == value.Length);
        // We look to see if the value implements IList, and if it does,
        // we set through that. 
        // 
        Debug.Assert(editValue is System.Collections.IList, "editValue is not an IList");
        if (editValue is ObjectList)
        {
          ObjectList list = (ObjectList)editValue;
          list.Clear();
          list.AddRange(value);
          return editValue;
        }
        if (editValue is System.Collections.IList)
        {
          System.Collections.IList list = (System.Collections.IList)editValue;

          list.Clear();
          for (int i = 0; i < value.Length; i++)
          {
            list.Add(value[i]);
          }
        }
      }
      return editValue;
    }
    private class StringCollectionForm : CollectionForm
    {

      private SplitContainer splitContainer1;
      private Label label1;
      private Button btnOk;
      private Splitter splitter1;
      private Button btnCancel;
      private TextBox richTextBox1;
      private Panel panel1;
      private StringCollectionEditor editor = null;


      public StringCollectionForm(CollectionEditor editor) : base(editor)
      {
        this.editor = (StringCollectionEditor)editor;
        InitializeComponent();
        richTextBox1.AcceptsTab = true;
        richTextBox1.WordWrap = false;
        richTextBox1.Multiline = true;
        HookEvents();
      }

      private void Edit1_keyDown(object sender, KeyEventArgs e)
      {
        if (e.KeyCode == Keys.Escape)
        {
          btnCancel.PerformClick();
          e.Handled = true;
        }
      }

      private void StringCollectionEditor_HelpButtonClicked(object sender, CancelEventArgs e)
      {
        e.Cancel = true;
        editor.ShowHelp();
      }

      private void HookEvents()
      {
        this.richTextBox1.KeyDown += new KeyEventHandler(this.Edit1_keyDown);
        this.btnOk.Click += new EventHandler(this.OKButton_click);
        //this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.StringCollectionEditor_HelpButtonClicked);
      }

      private void InitializeComponent()
      {
                this.splitContainer1 = new System.Windows.Forms.SplitContainer();
                this.label1 = new System.Windows.Forms.Label();
                this.btnOk = new System.Windows.Forms.Button();
                this.splitter1 = new System.Windows.Forms.Splitter();
                this.btnCancel = new System.Windows.Forms.Button();
                this.panel1 = new System.Windows.Forms.Panel();
                this.richTextBox1 = new System.Windows.Forms.TextBox();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
                this.splitContainer1.Panel1.SuspendLayout();
                this.splitContainer1.Panel2.SuspendLayout();
                this.splitContainer1.SuspendLayout();
                this.panel1.SuspendLayout();
                this.SuspendLayout();
                // 
                // splitContainer1
                // 
                this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainer1.IsSplitterFixed = true;
                this.splitContainer1.Location = new System.Drawing.Point(10, 5);
                this.splitContainer1.Name = "splitContainer1";
                this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainer1.Panel1
                // 
                this.splitContainer1.Panel1.Controls.Add(this.panel1);
                this.splitContainer1.Panel1.Controls.Add(this.label1);
                this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
                // 
                // splitContainer1.Panel2
                // 
                this.splitContainer1.Panel2.Controls.Add(this.btnOk);
                this.splitContainer1.Panel2.Controls.Add(this.splitter1);
                this.splitContainer1.Panel2.Controls.Add(this.btnCancel);
                this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
                this.splitContainer1.Size = new System.Drawing.Size(532, 314);
                this.splitContainer1.SplitterDistance = 282;
                this.splitContainer1.TabIndex = 0;
                // 
                // label1
                // 
                this.label1.Dock = System.Windows.Forms.DockStyle.Top;
                this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.label1.Location = new System.Drawing.Point(5, 5);
                this.label1.Name = "label1";
                this.label1.Size = new System.Drawing.Size(522, 21);
                this.label1.TabIndex = 0;
                this.label1.Text = "Enter the string in the collection (one per line):";
                // 
                // btnOk
                // 
                this.btnOk.Dock = System.Windows.Forms.DockStyle.Right;
                this.btnOk.Location = new System.Drawing.Point(326, 0);
                this.btnOk.Name = "btnOk";
                this.btnOk.Size = new System.Drawing.Size(88, 28);
                this.btnOk.TabIndex = 2;
                this.btnOk.Text = "OK";
                this.btnOk.UseVisualStyleBackColor = true;
                this.btnOk.Click += new System.EventHandler(this.OKButton_click);
                // 
                // splitter1
                // 
                this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
                this.splitter1.Location = new System.Drawing.Point(414, 0);
                this.splitter1.MaximumSize = this.splitter1.Size;
                this.splitter1.MinimumSize = new System.Drawing.Size(5, 0);
                this.splitter1.Name = "splitter1";
                this.splitter1.Size = new System.Drawing.Size(25, 28);
                this.splitter1.TabIndex = 1;
                this.splitter1.TabStop = false;
                // 
                // btnCancel
                // 
                this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
                this.btnCancel.Location = new System.Drawing.Point(439, 0);
                this.btnCancel.Name = "btnCancel";
                this.btnCancel.Size = new System.Drawing.Size(88, 28);
                this.btnCancel.TabIndex = 0;
                this.btnCancel.Text = "Cancel";
                this.btnCancel.UseVisualStyleBackColor = true;
                this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
                // 
                // panel1
                // 
                this.panel1.Controls.Add(this.richTextBox1);
                this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
                this.panel1.Location = new System.Drawing.Point(5, 26);
                this.panel1.Name = "panel1";
                this.panel1.Padding = new System.Windows.Forms.Padding(2);
                this.panel1.Size = new System.Drawing.Size(522, 251);
                this.panel1.TabIndex = 1;
                this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
                // 
                // richTextBox1
                // 
                this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
                this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
                this.richTextBox1.Location = new System.Drawing.Point(2, 2);
                this.richTextBox1.Name = "richTextBox1";
                this.richTextBox1.ScrollBars = ScrollBars.Both;
                this.richTextBox1.Size = new System.Drawing.Size(518, 247);
                this.richTextBox1.TabIndex = 2;
                this.richTextBox1.Text = "";
                this.richTextBox1.WordWrap = false;
                // 
                // StringCollectionEditor
                // 
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
                this.ClientSize = new System.Drawing.Size(552, 324);
                this.Controls.Add(this.splitContainer1);
                this.HelpButton = true;
                this.MaximizeBox = false;
                this.MinimizeBox = false;
                this.Name = "StringCollectionEditor";
                this.Padding = new System.Windows.Forms.Padding(10, 5, 10, 5);
                this.ShowIcon = false;
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
                this.Text = "String Collection Editor";
                this.splitContainer1.Panel1.ResumeLayout(false);
                this.splitContainer1.Panel2.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
                this.splitContainer1.ResumeLayout(false);
                this.panel1.ResumeLayout(false);
                this.ResumeLayout(false);
            }

       private void panel1_Paint(object sender, PaintEventArgs e)
       {
         using (Pen pen = new Pen(SystemColors.Highlight))
         {
           e.Graphics.DrawRectangle(pen, Rectangle.Inflate(panel1.DisplayRectangle, 1, 1));
         }
       }

      private void btnCancel_Click(object sender, EventArgs e)
      {
        DialogResult = DialogResult.Cancel;
        Close();
      }

      private void OKButton_click(object sender, EventArgs e)
      {
        char[] delims = new char[] { '\n' };
        char[] trims = new char[] { '\r' };

        string[] strings = richTextBox1.Text.Split(delims);
        object[] curItems = Items;

        int nItems = strings.Length;
        for (int i = 0; i < nItems; i++)
        {
          strings[i] = strings[i].Trim(trims);
        }

        bool dirty = true;
        if (nItems == curItems.Length)
        {
          int i;
          for (i = 0; i < nItems; ++i)
          {
            if (!strings[i].Equals((string)curItems[i]))
            {
              break;
            }
          }

          if (i == nItems)
            dirty = false;
        }

        if (!dirty)
        {
          DialogResult = DialogResult.Cancel;
          Close();
        }
        if (strings.Length > 0 && strings[strings.Length - 1].Length == 0)
        {
          nItems--;
        }

        object[] values = new object[nItems];
        for (int i = 0; i < nItems; i++)
        {
          values[i] = strings[i];
        }

        Items = values;
        DialogResult = DialogResult.OK;
        Close();
      }

      // <summary>
      //      This is called when the value property in the CollectionForm has changed. 
      //      In it you should update your user interface to reflect the current value.
      // </summary>
      // 
      protected override void OnEditValueChanged()
      {
        object[] items = Items;
        string text = string.Empty;

        for (int i = 0; i < items.Length; i++)
        {
          if (items[i] is string)
          {
            text += (string)items[i];
            if (i != items.Length - 1)
            {
              text += "\r\n";
            }
          }
        }

        richTextBox1.Text = text;
        richTextBox1.SelectionStart = 0;
        richTextBox1.SelectionLength = richTextBox1.TextLength;
      }
    }
  }
  #endregion
}
