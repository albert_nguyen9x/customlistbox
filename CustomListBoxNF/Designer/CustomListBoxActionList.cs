﻿using CustomListBoxNF.CustomControl;
using CustomListBoxNF.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.Design;

namespace CustomListBoxNF.Designer
{
  #region define smart tag for custom list box
  internal class CustomListBoxActionList : DesignerActionList
  {
    // == Component
    // ControlDesigner.Component = Control
    private CustomListBox customListBox;
    private DesignerActionUIService designerActionSvc = null;
    private DesignerActionItemCollection items;
    private CustomListBoxDesigner designer;

    private bool _dataBounded = false;
    //CustomListBoxDesigner Designer
    //{
    //  get
    //  {
    //    IDesignerHost designerHost = this.GetService(typeof(IDesignerHost)) as IDesignerHost;
    //    if (designerHost == null) return null;
    //    return (CustomListBoxDesigner)designerHost.GetDesigner(this.customListBox);
    //  }
    //}
    // DataBounded proxy property
    public bool DataBounded
    {
      get { return _dataBounded; }
      set
      {
        _dataBounded = value;
        RefreshPanelContent();
      }
    }
    [TypeConverter("System.Windows.Forms.Design.DataSourceConverter, System.Design"),
      AttributeProvider(typeof(IListSource))]
    public object DataSource
    {
      get { return this.customListBox.DataSource; }
      set
      {
        GetPropertyByName("DataSource").SetValue(customListBox, value);
      }
    }
    public string DisplayMember
    {
      get { return this.customListBox.DisplayMember; }
      set
      {
        GetPropertyByName("DisplayMember").SetValue(customListBox, value);
      }
    }
    public string ValueMember
    {
      get { return this.customListBox.ValueMember; }
      set
      {
        GetPropertyByName("ValueMember").SetValue(customListBox, value);
      }
    }
    public string SelectedValue
    {
      get => customListBox.SelectedValue;
      set
      {
        GetPropertyByName("SelectedValue").SetValue(customListBox, value);
      }
    }

    public CustomListBoxActionList(ControlDesigner designer) : base(designer.Component)
    {
      customListBox = (CustomListBox)Component;
      this.designer = (CustomListBoxDesigner)designer;
      designerActionSvc = (DesignerActionUIService)GetService(typeof(DesignerActionUIService));
      items = new DesignerActionItemCollection();
      DataBounded = customListBox.DataSource != null;
    }
    private void RefreshPanelContent()
    {
      if (designerActionSvc != null)
      {
        designerActionSvc.Refresh(customListBox);
      }
    }
    public override DesignerActionItemCollection GetSortedActionItems()
    {
      items.Clear();
      items.Add(new DesignerActionPropertyItem("DataBounded", "Use Data Bound Item", "Binding Mode", "When selected, option to setup data binding will be displayed"));
      if (!DataBounded)
      {
        items.Add(new DesignerActionHeaderItem("Unbound Mode"));
        items.Add(new DesignerActionMethodItem(this, "InvokeItemsDialog", "Edit Items", Helper.GetAttributeString<CategoryAttribute>(customListBox, "Items", "Category"), Helper.GetAttributeString<DescriptionAttribute>(customListBox, "Items", "Description"), true));
      }
      else
      {
        items.Add(new DesignerActionHeaderItem("Data Binding Mode"));
        items.Add(new DesignerActionPropertyItem("DataSource", "Data Source", Helper.GetAttributeString<CategoryAttribute>(customListBox, "DataSource", "Category"), Helper.GetAttributeString<DescriptionAttribute>(customListBox, "DataSource", "Description")));
        items.Add(new DesignerActionPropertyItem("DisplayMember", "Display Member", Helper.GetAttributeString<CategoryAttribute>(customListBox, "DisplayMember", "Category"), Helper.GetAttributeString<DescriptionAttribute>(customListBox, "DisplayMember", "Description")));
        items.Add(new DesignerActionPropertyItem("ValueMember", "Value Member", Helper.GetAttributeString<CategoryAttribute>(customListBox, "ValueMember", "Category"), Helper.GetAttributeString<DescriptionAttribute>(customListBox, "ValueMember", "Description")));
        items.Add(new DesignerActionPropertyItem("SelectedValue", "Selected Value", Helper.GetAttributeString<CategoryAttribute>(customListBox, "SelectedValue", "Category")));
      }
      return items;
      //return base.GetSortedActionItems();
    }
    public void InvokeItemsDialog()
    {
      var editorServiceContext = typeof(ControlDesigner).Assembly.GetTypes()
    .Where(x => x.Name == "EditorServiceContext").FirstOrDefault();
      var editValue = editorServiceContext.GetMethod("EditValue",
          System.Reflection.BindingFlags.Static |
          System.Reflection.BindingFlags.Public);
      editValue.Invoke(null, new object[] { designer, Component, "Items" });
    }
    private PropertyDescriptor GetPropertyByName(string propName)
    {
      PropertyDescriptor prop = TypeDescriptor.GetProperties(customListBox)[propName];
      if (prop == null)
      {
        throw new ArgumentException("Invalid Property", propName);
      }
      else
      {
        return prop;
      }
    }
  }
  #endregion
}
