﻿using CustomListBoxNF.Actions;
using CustomListBoxNF.CustomControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CustomListBoxNF
{
    internal class UndoRedoManager
    {
        private Stack<UndoableAction> _undoStack, _redoStack;
        private CustomListBox _component;
        public bool CanUndo { get => _undoStack != null && _undoStack.Count != 0; }
        public bool CanRedo { get => _redoStack != null && _redoStack.Count != 0; }
        public UndoRedoManager(CustomListBox component)
        {
            _undoStack = new Stack<UndoableAction>();
            _redoStack = new Stack<UndoableAction>();
            _component = component;
        }
        public bool Redo()
        {
            if (CanRedo)
            {
                return Redo(_redoStack.Pop());
            }
            return false;
        }
        private bool Redo(UndoableAction action)
        {
            bool res = action.PerformAction(_component);
            if (res)
            {
                _undoStack.Push(action);
            }
            return res;
        }
        public bool Undo()
        {
            if (CanUndo)
            {
                return Undo(_undoStack.Pop());
            }
            return false;
        }
        private bool Undo(UndoableAction action)
        {
            bool res = action.PerformUndo(_component);
            if (res)
            {
                _redoStack.Push(action);
            }
            return res;
        }
        public bool PerformAction(UndoableAction action)
        {
            bool res = action.PerformAction(_component);
            if (res)
            {
                _undoStack.Push(action);
                _redoStack.Clear();
            }
            return res;
        }
    }
}
