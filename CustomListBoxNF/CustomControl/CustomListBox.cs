﻿using CustomListBoxNF.Actions;
using CustomListBoxNF.Attributes;
using CustomListBoxNF.Designer;
using CustomListBoxNF.Model;
using CustomListBoxNF.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace CustomListBoxNF.CustomControl
{
    [ToolboxBitmap(typeof(ListBox)),
     DefaultEvent("SelectedIndexChanged"),
     DefaultProperty("Items"),
     Designer(typeof(CustomListBoxDesigner))]
    public partial class CustomListBox : Control, ISupportInitialize
    {
        private LinkedList<int> _indexSelectStack;
        private int _lastIndexSel = -1;
        private SelectionMode _selectionMode = SelectionMode.One;
        private bool _multiColumn;
        private int _columnWidth;
        private int _selectedIndex = -1;
        private object _selectedItem = null;
        private bool _scrollAlwayVisible;
        private int _itemHeight = 13;
        private bool _enableFormating = false;
        private string _formatString = "";
        private ScrollBar _mainScrollBar;
        private CustomHscrollBar _hScrollBar;
        private CustomVscrollbar _vScrollBar;
        private int _horizontalExtent = 0;
        private bool _horizontalScrollBar = false;

        private object _dataSource;
        internal PropertyDescriptorCollection _cachedPropertyDescs;
        private string _displayMember = "";
        private string _valueMember = "";
        private bool _sorted = false;
        private bool _ellipsisEnabled = false;
        private bool _editable = false;

        private int hScrollRealMax = 0;
        private int vScrollRealMax = 0;
        private int _editIndex = -1;
        private int _beforeEditScrollValue;
        private bool _integralHeight = true;
        private DrawMode _drawMode = DrawMode.Normal;

        private Rectangle _clientRect = Rectangle.Empty;


        private BorderStyle _borderStyle = BorderStyle.Fixed3D;
        private CurrencyManager _dataBindingManager;

        private ListChangedEventHandler _dataChangedHandle;


        public event EventHandler SelectedIndexChanged;
        public event EventHandler SelectedItemChanged;
        public event EventHandler DrawItem;
        public event EventHandler ValueMemberChanged;

        //hidden text box
        private TextBoxPadding _textBox;

        //hidden line indicator
        private MoveIndicator _indicator;
        private int _dragIndex;

        //Timer for Drag Drop Event
        private System.Windows.Forms.Timer _dragDropTimer;
        private int _milliMouse = 0;
        private Point _mouseDownPoint;

        //UndoRedoManager
        private UndoRedoManager _undoRedoMng;
        //Model
        private LayoutModel _layoutModel;
        private DataModel _dataModel;
        private IFormatData _formater;

        //Time out value
        private bool _exeScroll = false;
        private bool _exePaint = false;
        private bool _lastVscrollVisible, _lastHscrollVisible;
        private bool _internFocus = false;
        private int _focusIndex = 0;

        private bool initialize = false;
        private bool enable;
        //Default width listbox
        private int _defaultColumnWidth = 120;
        private System.Windows.Forms.Timer _transTimer;
        private bool _isTrans = false;
        private int _transValue = 0;
        private int _oldScrollVal;
        #region CustomListBox Properties

        internal Rectangle MainRect
        {
            get
            {
                if (_clientRect == Rectangle.Empty)
                {
                    _clientRect = Rectangle.Inflate(ClientRectangle, -(int)_borderStyle, -(int)_borderStyle);
                }
                return _clientRect;
            }
            set
            {
                //if (value.Height > Size.Height || value.Width > Size.Width) return;

                _clientRect = value;
            }
        }
        public new bool Enabled
        {
            get => base.Enabled;
            set
            {
                if (initialize)
                {
                    enable = value;
                    return;
                }
                base.Enabled = value;
            }
        }
        [DefaultValue(true), Category("Behavior"), LBDescriptionAttr("UseTabStopDesc")]
        public bool UseTabStops { get; set; } = true;
        [DefaultValue(true), Category("Behavior"), LBDescriptionAttr("IntegralHeightDesc")]
        public bool IntegralHeight
        {
            get => _integralHeight;
            set
            {
                if (_integralHeight != value)
                {
                    _integralHeight = value;
                }
            }
        }
        [DefaultValue(typeof(Size), "120, 95")]
        public new Size Size
        {
            get => base.Size;
            set
            {
                if (base.Size != value)
                {
                    base.Size = value;
                }
            }
        }
        private bool EditMode
        {
            get
            {
                if (_textBox != null) return _textBox.Visible;
                return false;
            }
            set
            {
                //if (!_editable) return;
                if (_textBox.Visible != value)
                {
                    _textBox.Visible = value;
                    if (value)
                    {
                        _textBox.KeyDown += TextBoxKeyDown;
                        _textBox.Focus();
                    }
                    else
                    {
                        _textBox.KeyDown -= TextBoxKeyDown;
                        _internFocus = true;
                        Focus();
                        _internFocus = false;
                    }
                }

            }
        }
        [DefaultValue(false), Category("Behavior"), LBDescriptionAttr("EditableDesc")]
        public bool Editable
        {
            get => _editable;
            set
            {
                if (_editable != value) _editable = value;
            }
        }
        //
        // Summary:
        //     Gets or sets the state when ellipsis(...) should be addition if text to long
        //
        // Returns:
        //     true to display ellipsis and false for otherwise. The
        //     default is false.
        [DefaultValue(false), Category("Behavior"), LBDescriptionAttr("EllipsisEnabledDesc")]
        public bool EllipsisEnabled
        {
            get => _ellipsisEnabled;
            set
            {
                _ellipsisEnabled = value;
                InvalidateScroll(false);
            }
        }
        //
        // Summary:
        //     Gets or sets the width by which the horizontal scroll bar of a System.Windows.Forms.ListBox
        //     can scroll.
        //
        // Returns:
        //     The width, in pixels, that the horizontal scroll bar can scroll the control.
        //     The default is zero.
        [DefaultValue(0), Category("Behavior"), LBDescriptionAttr("HorizontalExtentDesc")]
        public int HorizontalExtent
        {
            get => _horizontalExtent;
            set
            {
                _horizontalExtent = value;
                if (!_multiColumn && _horizontalScrollBar)
                {
                    //HorizontalScrollbar = true;
                    InvalidateScroll(false);
                }
            }
        }
        //
        // Summary:
        //     Gets or sets a value indicating whether a horizontal scroll bar is displayed
        //     in the control.
        //
        // Returns:
        //     true to display a horizontal scroll bar in the control; otherwise, false. The
        //     default is false.
        [DefaultValue(false), Category("Behavior"), LBDescriptionAttr("HorizontallScrollBar")]
        public bool HorizontalScrollbar
        {
            get => _horizontalScrollBar;
            set
            {
                _horizontalScrollBar = value;
                if (value && !_multiColumn && !_ellipsisEnabled && (_horizontalExtent == 0 || _horizontalExtent > MainRect.Width))
                {
                    _hScrollBar.Scroll -= handleTextScroll;
                    _hScrollBar.Scroll += handleTextScroll;
                }
                else
                {
                    _hScrollBar.Scroll -= handleTextScroll;
                }
                InvalidateScroll(true);
            }
        }
        //
        // Summary:
        //     Gets or sets a value indicating whether the items in the System.Windows.Forms.ListBox
        //     are sorted alphabetically.
        //
        // Returns:
        //     true if items in the control are sorted; otherwise, false. The default is false.
        [DefaultValue(false), Category("Behavior"), LBDescriptionAttr("SortedDesc")]
        public bool Sorted
        {
            get => _sorted;
            set
            {
                _sorted = value;
                if (_sorted && _dataModel.ItemCount() != 0)
                {
                    HandleSortAndRepaint();
                }
            }
        }
        //
        // Summary:
        //     Gets or sets the type of border that is drawn around the System.Windows.Forms.ListBox.
        //
        // Returns:
        //     One of the System.Windows.Forms.BorderStyle values. The default is System.Windows.Forms.BorderStyle.Fixed3D.
        //
        // Exceptions:
        //   T:System.ComponentModel.InvalidEnumArgumentException:
        //     The value is not one of the System.Windows.Forms.BorderStyle values.
        [DefaultValue(typeof(BorderStyle), "Fixed3D"), Category("Appearance"), LBDescriptionAttr("BorderStyleDesc")]
        public BorderStyle BorderStyle
        {
            get => _borderStyle;
            set
            {
                if (_borderStyle != value)
                {
                    _borderStyle = value;
                    MainRect = Rectangle.Inflate(ClientRectangle, -(int)_borderStyle, -(int)_borderStyle);
                    if (_hScrollBar.Visible)
                    {
                        InvalidMainH(true);
                    }
                    if (_vScrollBar.Visible)
                    {
                        InvalidMainV(true);
                    }
                    if (_multiColumn)
                    {
                        InvalidateHScrollRect();
                    }
                    else
                    {
                        InvalidateVScrollRect();
                        if (_horizontalScrollBar) InvalidateHScrollRect();
                    }
                    InvalidateScroll(true);
                }
            }
        }
        //
        // Summary:
        //     Gets or sets the drawing mode for the control.
        //
        // Returns:
        //     One of the System.Windows.Forms.DrawMode values representing the mode for drawing
        //     the items of the control. The default is DrawMode.Normal.
        //
        // Exceptions:
        //   T:System.ComponentModel.InvalidEnumArgumentException:
        //     The value assigned to the property is not a member of the System.Windows.Forms.DrawMode
        //     enumeration.
        //
        //   T:System.ArgumentException:
        //     A multicolumn System.Windows.Forms.ListBox cannot have a variable-sized height.
        [DefaultValue(typeof(DrawMode), "Normal"), Category("Behavior"), LBDescriptionAttr("DrawModeDesc")]
        public DrawMode DrawMode
        {
            get => _drawMode;
            set
            {
                if (_drawMode != value)
                {
                    if (value == DrawMode.OwnerDrawVariable && _multiColumn)
                    {
                        throw new ArgumentException("ListBox cannot have variable height and be multicolumn. Either make the ListBox owner-draw fixed height, or make the ListBox single column.Parameter name: value");
                    }
                    _drawMode = value;
                    InvalidateScroll(false);
                }
            }
        }
        //
        // Summary:
        //     Gets or sets the background image displayed in the control.
        //
        // Returns:
        //     An System.Drawing.Image that represents the image to display in the background
        //     of the control.
        [Browsable(false), DefaultValue(null)]
        public override Image BackgroundImage { get; set; }
        //
        // Summary:
        //     Gets or sets the background image layout as defined in the System.Windows.Forms.ImageLayout
        //     enumeration.
        //
        // Returns:
        //     One of the values of System.Windows.Forms.ImageLayout (System.Windows.Forms.ImageLayout.Center
        //     , System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ImageLayout.Stretch,
        //     System.Windows.Forms.ImageLayout.Tile, or System.Windows.Forms.ImageLayout.Zoom).
        //     System.Windows.Forms.ImageLayout.Tile is the default value.
        //
        // Exceptions:
        //   T:System.ComponentModel.InvalidEnumArgumentException:
        //     The specified enumeration value does not exist.
        [Browsable(false), DefaultValue(typeof(ImageLayout), "None")]
        public override ImageLayout BackgroundImageLayout { get; set; }
        //
        // Summary:
        //     Gets or sets a value indicating whether formatting is applied to the System.Windows.Forms.ListControl.DisplayMember
        //     property of the System.Windows.Forms.ListControl.
        //
        // Returns:
        //     true if formatting of the System.Windows.Forms.ListControl.DisplayMember property
        //     is enabled; otherwise, false. The default is false.
        [DefaultValue(false), LBDescriptionAttr("FormatingEnabledDesc")]
        public bool FormattingEnabled { get => _enableFormating; set => _enableFormating = value; }
        //
        // Summary:
        //     Gets or sets the format-specifier characters that indicate how a value is to
        //     be displayed.
        //
        // Returns:
        //     The string of format-specifier characters that indicates how a value is to be
        //     displayed.
        [DefaultValue(""), LBDescriptionAttr("FormatStringDesc"),
          Editor(typeof(FormatStringEditor), typeof(UITypeEditor)), TypeConverter(typeof(StringConverter))]
        public string FormatString
        {
            get => _formatString;
            set
            {
                if (value == null)
                {
                    value = "";
                }
                if (!value.Equals(_formatString))
                {
                    _formatString = value;
                    if (value != "")
                    {
                        _formater = FormatContainer.GetFormaterFromString(value);
                    }
                    else
                    {
                        _formater = FormatContainer.Default;
                    }
                    Invalidate();
                }
            }
        }
        //
        // Summary:
        //     Gets or sets the path of the property to use as the actual value for the items
        //     in the System.Windows.Forms.ListControl.
        //
        // Returns:
        //     A System.String representing a single property name of the System.Windows.Forms.ListControl.DataSource
        //     property value, or a hierarchy of period-delimited property names that resolves
        //     to a property name of the final data-bound object. The default is an empty string
        //     ("").
        //
        // Exceptions:
        //   T:System.ArgumentException:
        //     The specified property path cannot be resolved through the object specified by
        //     the System.Windows.Forms.ListControl.DataSource property.
        [Browsable(false), DefaultValue(""), Category("Data"), LBDescriptionAttr("ValueMemberDesc")]
        public string ValueMember
        {
            get => _valueMember;
            set
            {
                if (_valueMember != value)
                {
                    if (value != "" && !Helper.CheckPropertyValid(value, _dataBindingManager.GetItemProperties()))
                    {
                        throw new ArgumentException("Value member property data not valid");
                    }
                    _valueMember = value;
                    ValueMemberChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        //
        // Summary:
        //     Gets or sets the value of the member property specified by the System.Windows.Forms.ListControl.ValueMember
        //     property.
        //
        // Returns:
        //     An object containing the value of the member of the data source specified by
        //     the System.Windows.Forms.ListControl.ValueMember property.
        //
        // Exceptions:
        //   T:System.InvalidOperationException:
        //     The assigned value is null or the empty string ("").
        [Browsable(false), DefaultValue(""), Bindable(true), Category("Data")]
        public string SelectedValue { get; set; } = "";
        //
        // Summary:
        //     Gets or sets the data source for this System.Windows.Forms.ListControl.
        //
        // Returns:
        //     An object that implements the System.Collections.IList or System.ComponentModel.IListSource
        //     interfaces, such as a System.Data.DataSet or an System.Array. The default is
        //     null.
        //
        // Exceptions:
        //   T:System.ArgumentException:
        //     The assigned value does not implement the System.Collections.IList or System.ComponentModel.IListSource
        //     interfaces.
        [TypeConverter("System.Windows.Forms.Design.DataSourceConverter, System.Design"),
          RefreshProperties(RefreshProperties.Repaint),
          AttributeProvider(typeof(IListSource)),
          DefaultValue(null),
        Category("Data"), LBDescriptionAttr("DataSourceDesc")]
        public object DataSource
        {
            get => _dataSource;
            set
            {
                if (this._dataSource != value)
                {
                    if (value is DataSet set)
                    {
                        if (set.Tables.Count == 0)
                        {
                            return;
                        }
                        this._dataSource = set.Tables[0];
                    }
                    else
                    {
                        this._dataSource = value;
                    }
                    tryDataBinding();
                }
            }
        }
        //
        // Summary:
        //     Gets or sets the property to display for this System.Windows.Forms.ListControl.
        //
        // Returns:
        //     A System.String specifying the name of an object property that is contained in
        //     the collection specified by the System.Windows.Forms.ListControl.DataSource property.
        //     The default is an empty string ("").
        [Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design", typeof(UITypeEditor)),
          TypeConverter("System.Windows.Forms.Design.DataMemberFieldConverter, System.Design"),
          DefaultValue(""), Category("Data"), LBDescriptionAttr("DisplayMemberDesc")]
        public string DisplayMember
        {
            get => _displayMember;
            set
            {
                if (_dataBindingManager == null)
                {
                    throw new ArgumentException("Try set DataSource before using DisplayMember Property");
                }
                if (this._displayMember != value)
                {
                    //if (value != "" && !Helper.CheckPropertyValid(value, _dataBindingManager.GetItemProperties()))
                    //{
                    //    throw new ArgumentException("Display member property data not valid");
                    //}
                    this._displayMember = value;

                    tryDataBinding();
                }
            }
        }
        //
        // Summary:
        //     Gets or sets the text associated with this control.
        //
        // Returns:
        //     The text associated with this control.
        [Browsable(false), Bindable(false)]
        public override string Text { get => base.Text; set => base.Text = value; }
        //
        // Summary:
        //     Gets or sets a value indicating whether the vertical scroll bar is shown at all
        //     times.
        //
        // Returns:
        //     true if the vertical scroll bar should always be displayed; otherwise, false.
        //     The default is false.
        [DefaultValue(false), Category("Behavior"), LBDescriptionAttr("ScrollAlwayVisibleDesc")]
        public bool ScrollAlwaysVisible
        {
            get { return _scrollAlwayVisible; }
            set
            {
                _scrollAlwayVisible = value;
                //if (value == true) _scrollBar.Visible = value;
                //else _scrollBar.Visible = Items.Count * ItemHeight > Height;
                InvalidateScroll(true);
            }
        }
        //
        // Summary:
        //     When overridden in a derived class, gets or sets the zero-based index of the
        //     currently selected item.
        //
        // Returns:
        //     A zero-based index of the currently selected item. A value of negative one (-1)
        //     is returned if no item is selected.
        [Browsable(false), DefaultValue(-1), Bindable(true)]
        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                int oldIndex = _selectedIndex;
                _selectedIndex = value;
                if (oldIndex != _selectedIndex)
                {
                    SelectedIndexChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        //
        // Summary:
        //     Gets the items of the System.Windows.Forms.ListBox.
        //
        // Returns:
        //     An System.Windows.Forms.ListBox.ObjectCollection representing the items in the
        //     System.Windows.Forms.ListBox.
        [TypeConverter(typeof(CollectionConverter)),
          DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        /*Editor("System.Windows.Forms.Design.StringCollectionEditor, System.Design", typeof(UITypeEditor)),*/
        Editor(typeof(StringCollectionEditor), typeof(UITypeEditor)),
        Category("Data"), LBDescriptionAttr("ItemsDesc")]
        public ObjectList Items { get => _dataModel.Items; }
        //
        // Summary:
        //     Gets or sets the method in which items are selected in the System.Windows.Forms.ListBox.
        //
        // Returns:
        //     One of the System.Windows.Forms.SelectionMode values. The default is SelectionMode.One.
        //
        // Exceptions:
        //   T:System.ComponentModel.InvalidEnumArgumentException:
        //     The assigned value is not one of the System.Windows.Forms.SelectionMode values.
        [DefaultValue(typeof(SelectionMode), "One"), Category("Behavior"), LBDescriptionAttr("SelectionMode")]
        public SelectionMode SelectionMode
        {
            get { return _selectionMode; }
            set
            {
                int min = Math.Min((int)value, (int)_selectionMode);
                int max = Math.Max((int)value, (int)_selectionMode);
                if (min == 0 || (min < 2 && max > 1))
                {
                    _selectedIndex = -1;
                    _selectedItem = null;
                    SelectedIndices.Clear();
                    SelectedItems.Clear();
                    _indexSelectStack.Clear();
                    Invalidate();
                }
                _selectionMode = value;
            }
        }
        //
        // Summary:
        //     Gets or sets a value indicating whether the System.Windows.Forms.ListBox supports
        //     multiple columns.
        //
        // Returns:
        //     true if the System.Windows.Forms.ListBox supports multiple columns; otherwise,
        //     false. The default is false.
        //
        // Exceptions:
        //   T:System.ArgumentException:
        //     A multicolumn System.Windows.Forms.ListBox cannot have a variable-sized height.
        [DefaultValue(false), Category("Behavior"), LBDescriptionAttr("MultiColumDesc")]
        public bool MultiColumn
        {
            get => _multiColumn;
            set
            {
                _multiColumn = value;
                if (_mainScrollBar != null)
                {
                    _mainScrollBar.Scroll -= onScroll;
                }
                if (_multiColumn)
                {
                    _mainScrollBar = _hScrollBar;
                }
                else
                {
                    _mainScrollBar = _vScrollBar;
                }
                _mainScrollBar.Scroll += onScroll;
                InvalidateScroll(true);
            }
        }
        //
        // Summary:
        //     Gets or sets the width of columns in a multicolumn System.Windows.Forms.ListBox.
        //
        // Returns:
        //     The width, in pixels, of each column in the control. The default is 0.
        //
        // Exceptions:
        //   T:System.ArgumentException:
        //     A value less than zero is assigned to the property.
        [DefaultValue(0), Category("Behavior"), LBDescriptionAttr("ColumnWidthDesc")]
        public int ColumnWidth
        {
            get => _columnWidth;
            set
            {
                _columnWidth = value;
                if (_multiColumn)
                {
                    InvalidateScroll(true);
                }
            }
        }
        //
        // Summary:
        //     Gets or sets the currently selected item in the System.Windows.Forms.ListBox.
        //
        // Returns:
        //     An object that represents the current selection in the control.
        [Browsable(false), DefaultValue(null), Bindable(true)]
        public object SelectedItem
        {
            get => _selectedItem;
            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    SelectedItemChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        //
        // Summary:
        //     Gets a collection containing the currently selected items in the System.Windows.Forms.ListBox.
        //
        // Returns:
        //     A System.Windows.Forms.ListBox.SelectedObjectCollection containing the currently
        //     selected items in the control.
        [Browsable(false)]
        public List<object> SelectedItems { get; } = new List<object>();
        //
        // Summary:
        //     Gets a collection that contains the zero-based indexes of all currently selected
        //     items in the System.Windows.Forms.ListBox.
        //
        // Returns:
        //     A System.Windows.Forms.ListBox.SelectedIndexCollection containing the indexes
        //     of the currently selected items in the control. If no items are currently selected,
        //     an empty System.Windows.Forms.ListBox.SelectedIndexCollection is returned.
        [Browsable(false)]
        public List<int> SelectedIndices { get; } = new List<int>();
        //
        // Summary:
        //     Gets or sets the height of an item in the System.Windows.Forms.ListBox.
        //
        // Returns:
        //     The height, in pixels, of an item in the control.
        //
        // Exceptions:
        //   T:System.ArgumentOutOfRangeException:
        //     The System.Windows.Forms.ListBox.ItemHeight property was set to less than 0 or
        //     more than 255 pixels.
        [DefaultValue(13), Category("Behavior"), LBDescriptionAttr("ItemHeightDesc")]
        public int ItemHeight
        {
            get => _itemHeight;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Item height can't be negative");
                }
                int oldValue = _itemHeight;
                _itemHeight = value;
                _textBox.Height = _itemHeight;
                if (DrawMode == DrawMode.Normal)
                {
                    _itemHeight = oldValue;
                    return;
                }
                InvalidateScroll(true);
            }
        }
        #endregion
        #region CustomListBox Method
        //
        // Summary:
        //    List box constructor
        //
        public CustomListBox()
        {
            _layoutModel = new LayoutModel(this);
            _dataModel = new DataModel(this);
            _undoRedoMng = new UndoRedoManager(this);
            _dragDropTimer = new System.Windows.Forms.Timer();
            _dragDropTimer.Interval = 100;
            _dragDropTimer.Tick += DragDropTick;
            _indexSelectStack = new LinkedList<int>();
            _transTimer = new System.Windows.Forms.Timer();
            _transTimer.Tick += TransformationTick;
            SetStyle(ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.Selectable, true);
            UpdateStyles();
            InitializeComponent();
            enable = Enabled;
            SuspendLayout();
            _vScrollBar = new CustomVscrollbar(this);
            _vScrollBar.Visible = false;
            _vScrollBar.Maximum = 0;
            _vScrollBar.ValueChanged += OnScrollValueChanged;
            _vScrollBar.VisibleChanged += VscrollbarVisibleChanged;
            _vScrollBar.Dock = DockStyle.Right;
            _hScrollBar = new CustomHscrollBar(this);
            _hScrollBar.Visible = false;
            _hScrollBar.Maximum = 0;
            _hScrollBar.ValueChanged += OnScrollValueChanged;
            _hScrollBar.VisibleChanged += HscrollVisibleChanged;
            _hScrollBar.Dock = DockStyle.Bottom;
            Controls.Add(_hScrollBar);
            Controls.Add(_vScrollBar);
            BackColor = SystemColors.Window;
            ForeColor = SystemColors.WindowText;
            _textBox = new TextBoxPadding(3)
            {
                BackColor = this.BackColor,
                ForeColor = this.ForeColor,
                BorderStyle = BorderStyle.None,
                Font = this.Font,
                Height = ItemHeight,
                Width = this.Width,
                Visible = false
            };
            _indicator = new MoveIndicator { Visible = false, Height = 17 };
            Controls.Add(_textBox);
            Controls.Add(_indicator);
            //InvalidateScrollVisible();
            _dataChangedHandle = new ListChangedEventHandler(OnDataChangedHandle);
            ResumeLayout(false);
            Size = new Size(120, 95);
            MultiColumn = false;
            FormattingEnabled = true;

        }

        private void TransformationTick(object sender, EventArgs e)
        {
            bool endTranform = false;
            if (_multiColumn)
            {
                if (_transValue >= _layoutModel.ColumnWidth)
                {
                    endTranform = true;
                }
                else
                {
                    _transValue = Math.Min(_layoutModel.ColumnWidth, _transValue + _transTimer.Interval);
                }
            }
            else
            {
                if (_transValue >= ItemHeight)
                {
                    endTranform = true;
                }
                else
                {
                    _transValue = Math.Min(ItemHeight, _transValue + _transTimer.Interval);
                }
            }
            if (endTranform)
            {
                _transTimer.Stop();
                _transValue = 0;
                _isTrans = false;
            }
            Invalidate();
            Update();
        }

        public void BeginInit()
        {
            initialize = true;
        }

        public void EndInit()
        {
            initialize = false;
            base.Enabled = enable;
        }
        #region observer method
        private void VscrollbarVisibleChanged(object sender, EventArgs e)
        {
            InvalidateHScrollRect();
            if (_lastVscrollVisible != _vScrollBar.Visible)
            {
                InvalidMainV(_vScrollBar.Visible);
                _lastVscrollVisible = _vScrollBar.Visible;
            }
        }
        private void HscrollVisibleChanged(object sender, EventArgs e)
        {
            InvalidateVScrollRect();
            if (_lastHscrollVisible != _hScrollBar.Visible)
            {
                InvalidMainH(_hScrollBar.Visible);
                _lastHscrollVisible = _hScrollBar.Visible;
            }
        }
        private void DragDropTick(object sender, EventArgs e)
        {
            _milliMouse += _dragDropTimer.Interval;
            if (_milliMouse >= 300)
            {
                //Stop and do drag and drop
                _dragDropTimer.Stop();
                _milliMouse = 0;
                _indicator.Visible = false;
                HandleDragDrop();
            }
        }
        private void OnScrollValueChanged(object sender, EventArgs e)
        {
            if (EditMode && _mainScrollBar.Value != _beforeEditScrollValue)
            {
                _mainScrollBar.Value = _beforeEditScrollValue;
            }
            if (_multiColumn)
            {
                int colSize = _layoutModel.ColumnSize;
                if (_mainScrollBar.Value % colSize != 0)
                {
                    _mainScrollBar.Value = _mainScrollBar.Value / colSize * colSize;
                }
            }
        }
        //private void ItemListChanged(object sender, EventArgs e)
        //{
        //  if (_horizontalScrollBar)
        //  {
        //    HorizontalScrollbar = _horizontalScrollBar;
        //  }
        //  else
        //  {
        //    if (RightToLeft == RightToLeft.Yes)
        //    {
        //      recalculateTextWidth();
        //    }
        //    InvalidateScroll(_multiColumn);
        //  }
        //}
        private void onScroll(object sender, ScrollEventArgs e)
        {
            if (EditMode || !Enabled || e.NewValue == e.OldValue) return;
            if (_multiColumn)
            {
                int colSize = _layoutModel.ColumnSize;
                if (e.NewValue % colSize == 0)
                {
                    if (_transTimer.Enabled)
                    {
                        _transTimer.Stop();
                    }
                    _transValue = 0;
                    _transTimer.Interval = (int)(_layoutModel.ColumnWidth * 0.15f);
                    _isTrans = true;
                    _oldScrollVal = e.OldValue;
                    _transTimer.Start();
                }
            }
            else
            {
                if (_transTimer.Enabled)
                {
                    _transTimer.Stop();
                }
                _transValue = 0;
                _transTimer.Interval = (int)(_itemHeight * 0.10f);
                _isTrans = true;
                _transTimer.Start();
                Invalidate();
            }
        }
        private void handleTextScroll(object sender, ScrollEventArgs e)
        {
            Invalidate();
        }
        #endregion


        private void HandleDragDrop()
        {
            Point screenCoords = Cursor.Position;
            Point controlPoint = PointToClient(screenCoords);
            if (_mouseDownPoint != controlPoint) return;

            int firstIndex = _mainScrollBar.Value;
            //if (_multiColumn)
            //{
            //    firstIndex /= colSize;
            //}
            int index = _layoutModel.GetIndexFromLocation(controlPoint, firstIndex);
            if (index >= 0 && index < _dataModel.ItemCount())
            {
                Debug.WriteLine($"Drag drop at {index}");
                _dragIndex = index;
                _indicator.Text = getTextDisplay(Items[index]);
                _indicator.Visible = true;
                DoDragDrop(Items[index], DragDropEffects.Move);
                OnDragOver(new DragEventArgs(null, 0, screenCoords.X, screenCoords.Y, DragDropEffects.Move, DragDropEffects.Move));
            }
        }


        private Size getScrollSize(ScrollBar scrollBar)
        {
            if (!scrollBar.Visible) return Size.Empty;

            return scrollBar.Size;
        }
        private void HandleSortAndRepaint()
        {
            _dataModel.Sort();
            if ((int)_selectionMode > 0)
            {
                if (_selectionMode == SelectionMode.One)
                {
                    _selectedIndex = _dataModel.IndexOf(_selectedItem);
                }
                else
                {
                    SelectedIndices.Clear();
                    foreach (object o in SelectedItems)
                    {
                        SelectedIndices.Add(_dataModel.IndexOf(o));
                    }
                }
            }
            Invalidate();
        }

        //private void recalculateTextWidth()
        //{
        //  int width;
        //  bool textLong = enableIfItemLong(out width);
        //  if (textLong)
        //  {
        //    _fitWidthReal = (width - MainRect.Width + getScrollSize(_vScrollBar).Width);
        //  }
        //}

        private void OnDataChangedHandle(object sender, ListChangedEventArgs e)
        {
            _dataModel.OnBindingDataChanged(e, _dataBindingManager);
        }

        private void tryDataBinding()
        {
            if (DataSource == null || base.BindingContext == null) return;

            CurrencyManager binding = (CurrencyManager)base.BindingContext[_dataSource];
            if (_dataBindingManager != binding)
            {
                if (_dataBindingManager != null)
                {
                    _dataBindingManager.ListChanged -= _dataChangedHandle;
                }
                _dataBindingManager = binding;
                if (_dataBindingManager != null)
                {
                    if (_displayMember != "" && !Helper.CheckPropertyValid(_displayMember, _dataBindingManager.GetItemProperties()))
                    {
                        throw new ArgumentException("Display member property data not valid");
                    }
                    if (_valueMember != "" && !Helper.CheckPropertyValid(_valueMember, _dataBindingManager.GetItemProperties()))
                    {
                        throw new ArgumentException("Value member property data not valid");
                    }
                    _dataBindingManager.ListChanged += _dataChangedHandle;
                    _dataModel.RefreshBindingData(_dataBindingManager);
                    _cachedPropertyDescs = binding.GetItemProperties();
                }

            }
        }

        private void InvalidateScrollVisible()
        {
            bool hScrollNeedVisible, vScrollNeedVisible;

            if (!Visible)
            {
                hScrollNeedVisible = vScrollNeedVisible = false;
            }
            else
            {
                if (!_multiColumn)
                {
                    if (!_ellipsisEnabled && _horizontalScrollBar && (_horizontalExtent == 0 || _horizontalExtent > MainRect.Width))
                    {
                        hScrollNeedVisible = _scrollAlwayVisible | _dataModel.ItemLongest > MainRect.Width;
                    }
                    else
                    {
                        hScrollNeedVisible = false;
                    }
                    int deltaHeight = hScrollNeedVisible && !_hScrollBar.Visible ? _hScrollBar.Height : 0;
                    vScrollNeedVisible = _scrollAlwayVisible || Items.Count > (MainRect.Height - deltaHeight) / ItemHeight;
                }
                else
                {
                    vScrollNeedVisible = false;
                    if (_columnWidth <= 0)
                    {
                        hScrollNeedVisible = _scrollAlwayVisible || Items.Count > MainRect.Height / ItemHeight;

                    }
                    else
                    {
                        hScrollNeedVisible = _scrollAlwayVisible || Items.Count > MainRect.Height / ItemHeight * (MainRect.Width / _columnWidth);
                    }
                }
            }
            if (_vScrollBar.Visible != vScrollNeedVisible)
            {
                _vScrollBar.Visible = vScrollNeedVisible;
            }
            if (_hScrollBar.Visible != hScrollNeedVisible)
            {
                _hScrollBar.Visible = hScrollNeedVisible;
            }
        }

        private void InvalidMainH(bool visible)
        {
            if (RightToLeft == RightToLeft.No)
            {
                if (visible) MainRect = new Rectangle(MainRect.X, MainRect.Y, MainRect.Width, MainRect.Height - _hScrollBar.Height);
                else MainRect = new Rectangle(MainRect.X, MainRect.Y, MainRect.Width, MainRect.Height + _hScrollBar.Height);
            }
            else
            {
                if (visible) MainRect = new Rectangle(MainRect.X, MainRect.Y, MainRect.Width, MainRect.Height - _hScrollBar.Height);
                else MainRect = new Rectangle(MainRect.X, MainRect.Y, MainRect.Width, MainRect.Height + _hScrollBar.Height);
            }
        }

        private void InvalidMainV(bool visible)
        {
            if (RightToLeft == RightToLeft.No)
            {
                if (visible) MainRect = new Rectangle(MainRect.X, MainRect.Y, MainRect.Width - _vScrollBar.Width, MainRect.Height);
                else MainRect = new Rectangle(MainRect.X, MainRect.Y, MainRect.Width + _vScrollBar.Width, MainRect.Height);
            }
            else
            {
                if (visible) MainRect = new Rectangle(MainRect.X + _vScrollBar.Width, MainRect.Y, MainRect.Width - _vScrollBar.Width, MainRect.Height);
                else MainRect = new Rectangle(MainRect.X - _vScrollBar.Width, MainRect.Y, MainRect.Width + _vScrollBar.Width, MainRect.Height);
            }
        }

        //private bool enableIfItemLong(out int longestTextWidth)
        //{
        //  bool answ = false;
        //  longestTextWidth = 0;
        //  foreach (object o in Items)
        //  {
        //    Size textSize = TextRenderer.MeasureText(getTextDisplay(o), Font);
        //    if (textSize.Width > (MainRect.Width - getScrollSize(_vScrollBar).Width))
        //    {
        //      answ = true;
        //      longestTextWidth = Math.Max(longestTextWidth, textSize.Width);
        //    }
        //  }
        //  return answ;
        //}

        private void InvalidateVScrollRect()
        {
            int stX = (int)_borderStyle;
            Point newVscrollLocation;
            Size newVscrollSize;
            if (RightToLeft == RightToLeft.Yes)
            {
                newVscrollLocation = new Point(stX, stX);
                MainRect = new Rectangle(stX + getScrollSize(_vScrollBar).Width, MainRect.Y, MainRect.Width, MainRect.Height);
            }
            else
            {
                newVscrollLocation = new Point(Width - SystemInformation.VerticalScrollBarWidth - stX, stX);
                MainRect = new Rectangle(stX, MainRect.Y, MainRect.Width, MainRect.Height);
            }
            int height = Height - 2 * stX;
            if (_hScrollBar.Visible)
            {
                height -= SystemInformation.VerticalScrollBarWidth;
            }
            newVscrollSize = new Size(SystemInformation.VerticalScrollBarWidth, height);
            if (_vScrollBar.Location != newVscrollLocation)
            {
                _vScrollBar.Location = newVscrollLocation;
            }
            if (_vScrollBar.Size != newVscrollSize)
            {
                _vScrollBar.Size = newVscrollSize;
            }
        }
        private void InvalidateHScrollRect()
        {
            int stX = (int)_borderStyle;
            Point newHscrollLocation;
            Size newHscrollSize; ;
            if (RightToLeft == RightToLeft.Yes)
            {
                newHscrollLocation = new Point(stX + getScrollSize(_vScrollBar).Width, Height - SystemInformation.VerticalScrollBarWidth - stX);
            }
            else
            {
                newHscrollLocation = new Point(stX, Height - SystemInformation.HorizontalScrollBarHeight - stX);
            }
            newHscrollSize = new Size(Width - getScrollSize(_vScrollBar).Width - 2 * stX, SystemInformation.HorizontalScrollBarHeight);

            if (_hScrollBar.Location != newHscrollLocation)
            {
                _hScrollBar.Location = newHscrollLocation;
            }
            if (_hScrollBar.Size != newHscrollSize)
            {
                _hScrollBar.Size = newHscrollSize;
            }
        }
        internal void InvalidateScroll(bool recreateRec)
        {

            InvalidateScrollVisible();
            if (recreateRec)
            {
                int itemWidth = MainRect.Width;
                if (_multiColumn)
                {
                    if (_columnWidth != 0)
                    {
                        itemWidth = _columnWidth;
                    }
                    else
                    {
                        itemWidth = Math.Min(_defaultColumnWidth + (int)_borderStyle, itemWidth);
                    }
                }
                _layoutModel.ReCached(_dataModel.ItemCount(), ItemHeight, itemWidth);
            }
            InvalidateScrollValue();
            if (_sorted)
            {
                HandleSortAndRepaint();
            }
            else Invalidate();
            Update();
        }

        private void InvalidateScrollValue()
        {


            if (!_multiColumn)
            {
                int pageSize = _layoutModel.PageSize;
                if (pageSize == 0)
                {
                    return;
                }
                if (_vScrollBar.Visible)
                {
                    vScrollRealMax = _dataModel.ItemCount() - pageSize;
                    if (vScrollRealMax <= 0)
                    {
                        _vScrollBar.Enabled = false;
                    }
                    else
                    {
                        _vScrollBar.Enabled = true;
                        _vScrollBar.Maximum = vScrollRealMax + (pageSize - 1) - 1;
                        _vScrollBar.SmallChange = 1;
                        _vScrollBar.LargeChange = pageSize - 1;
                        if (_vScrollBar.Value > vScrollRealMax)
                        {
                            _vScrollBar.Value = vScrollRealMax;
                        }
                    }
                }
                if (_hScrollBar.Visible)
                {
                    if (_dataModel.ItemLongest < MainRect.Width || DrawMode != DrawMode.Normal)
                    {
                        _hScrollBar.Enabled = false;
                        _hScrollBar.Invalidate();
                    }
                    else
                    {
                        _hScrollBar.Enabled = true;
                        int deltaWidth = _dataModel.ItemLongest - Math.Max(1, MainRect.Width / 6 * 5);
                        _hScrollBar.Maximum = deltaWidth + MainRect.Width / 6 * 5;
                        _hScrollBar.SmallChange = TextRenderer.MeasureText("a", Font).Width;
                        _hScrollBar.LargeChange = Math.Max(1, MainRect.Width / 6 * 5);
                    }

                }
            }
            else if (_multiColumn && _hScrollBar.Visible)
            {
                int columnSize = _layoutModel.ColumnSize;
                int pageSize = _layoutModel.PageSize;
                if (columnSize == 0 || pageSize == 0)
                {
                    return;
                }
                hScrollRealMax = _dataModel.ItemCount() / pageSize;
                if (_dataModel.ItemCount() % pageSize == 0)
                {
                    hScrollRealMax--;
                }
                if (hScrollRealMax <= 0)
                {
                    _hScrollBar.Enabled = false;
                }
                else
                {
                    _hScrollBar.Enabled = true;
                    _hScrollBar.Maximum = hScrollRealMax * pageSize + pageSize - 1;
                    _hScrollBar.SmallChange = columnSize;
                    _hScrollBar.LargeChange = pageSize;
                }
            }
        }

        private void DrawBackground(Graphics graphics)
        {
            //switch (this.BorderStyle)
            //{
            //  case BorderStyle.FixedSingle:
            //    ControlPaint.DrawBorder(graphics, this.ClientRectangle, Color.Gray, ButtonBorderStyle.Solid);
            //    break;
            //  case BorderStyle.Fixed3D:
            //    ControlPaint.DrawBorder3D(graphics, this.ClientRectangle, Border3DStyle.Flat);
            //    break;
            //}
            graphics.FillRectangle(new SolidBrush(SystemColors.Control), ClientRectangle);
            if (_borderStyle != BorderStyle.None)
            {
                graphics.DrawRectangle(Pens.Gray, 0, 0, Width - 1, Height - 1);
            }
            using (var bg = new SolidBrush(BackColor))
            {
                graphics.FillRectangle(bg, MainRect);
            }
        }

        internal string getTextDisplay(object item)
        {
            if (_dataSource == null)
            {
                return item.ToString();
            }
            if (_displayMember != "")
            {
                item = Helper.GetPropertyValueFromDataSource(_displayMember, _cachedPropertyDescs, item);
            }

            if (_enableFormating && _formatString != "")
            {
                string formatString;
                if (_formater.TryConvertToString(item, out formatString))
                {
                    return formatString;
                }
            }
            return item.ToString();
        }

        private void PasteContextClick(object sender, EventArgs e)
        {
            int firstIndex = _mainScrollBar.Value;
            //if (_multiColumn)
            //{
            //    firstIndex /= colSize;
            //}
            int index = _layoutModel.GetIndexFromLocation(_mouseDownPoint, firstIndex);
            //int lastVisbileIndex = Math.Min(firstIndex + numItem, _dataModel.ItemCount());
            if (index >= 0 && index < _dataModel.ItemCount())
            {
                PasteItemsAction command = new PasteItemsAction(this, index, Clipboard.GetText());
                _undoRedoMng.PerformAction(command);
            }
        }

        private void CopyContextClick(object sender, EventArgs e)
        {
            switch (_selectionMode)
            {
                case SelectionMode.One:
                    Clipboard.SetText(getTextDisplay(SelectedItem));
                    break;
                case SelectionMode.MultiExtended:
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (object o in SelectedItems)
                    {
                        stringBuilder.Append(getTextDisplay(o));
                        stringBuilder.Append(Environment.NewLine);
                    }
                    Clipboard.SetText(stringBuilder.ToString());
                    break;
            }
        }

        private void addRangeSelection()
        {
            int min = Math.Min(_indexSelectStack.Last.Value, _lastIndexSel);
            int max = Math.Max(_indexSelectStack.Last.Value, _lastIndexSel);
            if (min == _indexSelectStack.Last.Value)
            {
                min++;
            }
            else
            {
                max--;
            }
            for (int i = min; i <= max; i++)
            {
                SelectedIndices.Add(i);
                SelectedItems.Add(Items[i]);
            }
        }

        private void removeRangeLastSelection()
        {
            int min = Math.Min(_indexSelectStack.Last.Value, _lastIndexSel);
            int max = Math.Max(_indexSelectStack.Last.Value, _lastIndexSel);
            if (min == _indexSelectStack.Last.Value)
            {
                min++;
            }
            else
            {
                max--;
            }
            for (int i = min; i <= max; i++)
            {
                if (!_indexSelectStack.Contains(i))
                {
                    SelectedIndices.Remove(i);
                    SelectedItems.Remove(Items[i]);
                }
            }
        }

        private void EnableEditAt(int index)
        {
            if (!_exePaint) return;
            EnsureVisible(index);
            int tbWidth = MainRect.Width;
            SuspendLayout();
            Rectangle itemRect = _layoutModel.GetRect(index - _mainScrollBar.Value);
            _textBox.Location = new Point(itemRect.X, itemRect.Y);
            if (_multiColumn)
            {
                tbWidth = _layoutModel.ColumnWidth;
            }
            _textBox.Width = tbWidth;
            _textBox.Text = getTextDisplay(Items[index]);
            _textBox.SelectionStart = 0;
            _textBox.SelectionLength = _textBox.Text.Length;
            _editIndex = index;
            ResumeLayout();
            EditMode = true;
            _beforeEditScrollValue = _mainScrollBar.Value;
        }

        private void EnsureVisible(int index)
        {
            if (index < 0)
            {
                index = 0;
            }
            else if (index >= _dataModel.ItemCount())
            {
                index = _dataModel.ItemCount() - 1;
            }
            int firstIndex;
            int columnWidth = _columnWidth == 0 ? MainRect.Width : _columnWidth;
            int pageSize = _layoutModel.PageSize;
            int colSize = _layoutModel.ColumnSize;
            while (true)
            {
                firstIndex = _mainScrollBar.Value;
                //if (_multiColumn)
                //{
                //    firstIndex /= colSize;
                //}
                if (index < firstIndex)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        _mainScrollBar.Value = Math.Max(_mainScrollBar.Minimum, _mainScrollBar.Value - _mainScrollBar.SmallChange);
                    });

                }
                else if (index >= (firstIndex + pageSize))
                {
                    Invoke((MethodInvoker)delegate
                    {
                        _mainScrollBar.Value = Math.Min(_mainScrollBar.Maximum, _mainScrollBar.Value + _mainScrollBar.SmallChange);
                    });
                }
                else break;
            }
            Invalidate();
        }


        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            InputMapAction.GetMap(InputMapType.EditorFocus)?
                .GetAction(new KeyStroke(e.KeyCode, ModifierKeys))?
                .PerformAction(this);
        }


        internal void ChangeValueAt(int index, object data)
        {
            if (_dataSource == null)
            {
                _dataModel.Replace(index, data);
            }
            else
            {
                if (_dataSource is IList list)
                {
                    object curData = _dataModel.Get(index);
                    if (_cachedPropertyDescs.Count == 0 || curData is string)
                    {
                        if (curData.GetType() != data.GetType())
                        {
                            data = Convert.ChangeType(data, curData.GetType());
                        }
                        list[index] = data;
                    }
                    else
                    {
                        Helper.SetPropertyValueFromDataSource(_displayMember, _cachedPropertyDescs, list[index], data);
                    }
                }
                else if (_dataSource is IListSource listSource)
                {
                    IList dataList = listSource.GetList();
                    Helper.SetPropertyValueFromDataSource(_displayMember, _cachedPropertyDescs, dataList[index], data);
                }
            }

            if (_selectionMode == SelectionMode.One)
            {
                SelectedIndex = index;
                SelectedItem = data;
            }
            else if (_selectionMode != SelectionMode.None)
            {
                SelectedIndices.Clear();
                SelectedItems.Clear();
                SelectedIndices.Add(index);
                SelectedItems.Add(data);
            }
            _indexSelectStack.Remove(index);
            _indexSelectStack.AddLast(index);

            InvalidateScroll(false);
        }

        internal void ChangeIndex(int fromIndex, int targetIndex)
        {
            _dataModel.MoveItem(fromIndex, targetIndex);
            SelectedIndex = targetIndex;
            SelectedItem = _dataModel.Get(targetIndex);
            _indexSelectStack.Remove(targetIndex);
            _indexSelectStack.AddLast(targetIndex);
            EnsureVisible(targetIndex);
        }
        private void ExecuteVisibleNextScroll(int index, Point location, int firstIndex, int lastIndex)
        {
            new Thread(() =>
            {
                _exeScroll = true;
                if (index < firstIndex || index >= lastIndex)
                {
                    EnsureVisible(index);
                }
                else
                {
                    if (!_multiColumn)
                    {
                        if (location.Y <= 1)
                        {
                            EnsureVisible(index - 1);
                        }
                        else if (location.Y >= Height - 1)
                        {
                            EnsureVisible(index + 1);
                        }
                    }
                    else if (location.X <= 1)
                    {
                        int numPerCol = _layoutModel.ColumnSize;
                        EnsureVisible(index - numPerCol);
                    }
                }
                Thread.Sleep(700);
                _exeScroll = false;
            }).Start();

        }
        #region protected override method
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            InputMapAction.GetMap(InputMapType.Focus)?
                .GetAction(new KeyStroke(e.KeyCode, ModifierKeys))?
                .PerformAction(this);
        }
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (_mainScrollBar == null || EditMode || !Enabled) return;

            if (!_multiColumn)
            {
                if (e.Delta < 0)
                {
                    _mainScrollBar.Value = Math.Min(vScrollRealMax, _mainScrollBar.Value + _mainScrollBar.LargeChange);
                }
                else
                    _mainScrollBar.Value = Math.Max(0, _mainScrollBar.Value - _mainScrollBar.LargeChange);
            }
            else
            {
                if (e.Delta < 0)
                {
                    _mainScrollBar.Value = Math.Min(hScrollRealMax, _mainScrollBar.Value + _mainScrollBar.LargeChange);
                }
                else
                    _mainScrollBar.Value = Math.Max(0, _mainScrollBar.Value - _mainScrollBar.LargeChange);
            }
            Invalidate();
            base.OnMouseWheel(e);
        }
        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Right:
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                    return true;
                case Keys.Shift | Keys.Right:
                case Keys.Shift | Keys.Left:
                case Keys.Shift | Keys.Up:
                case Keys.Shift | Keys.Down:
                    return true;
            }
            return base.IsInputKey(keyData);
        }
        protected override void OnEnabledChanged(EventArgs e)
        {
            SetStyle(ControlStyles.Selectable, Enabled);
            UpdateStyles();
            _vScrollBar.EnableChanged(Enabled);
            _hScrollBar.EnableChanged(Enabled);
            InvalidateScrollValue();
            Invalidate(true);
        }
        protected override void OnRightToLeftChanged(EventArgs e)
        {
            base.OnRightToLeftChanged(e);
            //cause base.OnRightToLeft set instead
            //_hScrollBar.RightToLeft = RightToLeft;
            //_vScrollBar.RightToLeft = RightToLeft;
            //_textBox.RightToLeft = RightToLeft;
            InvalidateVScrollRect();
            InvalidateHScrollRect();
            InvalidateScroll(true);
        }
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if (_selectionMode == SelectionMode.None || !_exePaint) return;

            //int firstIndex, numItem;
            //int columnWidth = _columnWidth <= 0 ? MainRect.Width : _columnWidth;
            //_layoutModel.GetCurrentBoundRange(_mainScrollBar.Value, columnWidth, out firstIndex, out numItem);
            int index = _layoutModel.GetIndexFromLocation(e.Location, _mainScrollBar.Value);

            if (index >= 0 && index < _dataModel.ItemCount())
            {
                if (EditMode)
                {
                    EditMode = false;
                }
                else
                {
                    _internFocus = true;
                    Focus();
                    _internFocus = false;
                }
                if (e.Button == MouseButtons.Left)
                {
                    switch (_selectionMode)
                    {
                        case SelectionMode.One:
                            //if (_indexSelectStack.Count != 0) _indexSelectStack.RemoveLast();
                            _indexSelectStack.Remove(index);
                            _indexSelectStack.AddLast(index);
                            SelectedItem = _dataModel.Get(index);
                            SelectedIndex = index;
                            if (_dataBindingManager != null && _valueMember != "")
                            {
                                SelectedValue = Helper.GetPropertyValueFromDataSource(_valueMember, _cachedPropertyDescs, SelectedItem).ToString();
                            }
                            _focusIndex = index;
                            break;
                        case SelectionMode.MultiSimple:
                            _internFocus = true;
                            if (SelectedIndices.Contains(index))
                            {
                                SelectedIndices.Remove(index);
                                SelectedItems.Remove(Items[index]);
                                _indexSelectStack.Remove(index);
                            }
                            else
                            {
                                _indexSelectStack.Remove(index);
                                _indexSelectStack.AddLast(index);
                                SelectedIndices.Add(index);
                                SelectedItems.Add(Items[index]);
                            }
                            break;
                        default:
                            if ((ModifierKeys & Keys.Shift) == Keys.Shift)
                            {
                                if (_indexSelectStack.Count == 0)
                                {
                                    return;
                                }
                                if (_lastIndexSel != -1)
                                {
                                    removeRangeLastSelection();
                                }
                                _lastIndexSel = index;
                                _focusIndex = _lastIndexSel;
                                addRangeSelection();
                            }
                            else
                            {
                                _lastIndexSel = -1;
                                if ((ModifierKeys & Keys.Control) == Keys.Control)
                                {
                                    if (SelectedIndices.Contains(index))
                                    {
                                        SelectedIndices.Remove(index);
                                        SelectedItems.Remove(Items[index]);
                                        _indexSelectStack.Remove(index);
                                    }
                                    else
                                    {
                                        _indexSelectStack.Remove(index);
                                        _indexSelectStack.AddLast(index);
                                        SelectedIndices.Add(index);
                                        SelectedItems.Add(Items[index]);
                                    }
                                }
                                else
                                {
                                    SelectedIndices.Clear();
                                    SelectedItems.Clear();
                                    _indexSelectStack.Clear();
                                    SelectedIndices.Add(index);
                                    SelectedItems.Add(Items[index]);
                                    _indexSelectStack.AddLast(index);
                                }
                                _focusIndex = index;
                            }
                            break;
                    }
                    EnsureVisible(index);
                    _internFocus = false;
                }
                else if (e.Button == MouseButtons.Right)
                {
                    switch (_selectionMode)
                    {
                        case SelectionMode.One:
                            //if (_indexSelectStack.Count != 0) _indexSelectStack.RemoveLast();
                            _indexSelectStack.Remove(index);
                            _indexSelectStack.AddLast(index);
                            _selectedItem = _dataModel.Get(index);
                            _selectedIndex = index;
                            if (_dataBindingManager != null && _valueMember != "")
                            {
                                SelectedValue = Helper.GetPropertyValueFromDataSource(_valueMember, _cachedPropertyDescs, _selectedItem).ToString();
                            }
                            Invalidate();
                            break;
                        case SelectionMode.MultiExtended:
                            if (!SelectedIndices.Contains(index))
                            {
                                SelectedIndices.Add(index);
                                SelectedItems.Add(_dataModel.Get(index));
                            }
                            Invalidate();
                            break;
                    }
                    ContextMenu contextMenu = new ContextMenu();
                    MenuItem CopyItem = new MenuItem("Copy", CopyContextClick);
                    contextMenu.MenuItems.Add(CopyItem);
                    if (Clipboard.ContainsText(TextDataFormat.Text))
                    {
                        MenuItem PasteItem = new MenuItem($"Paste from {index + 1}", PasteContextClick);
                        contextMenu.MenuItems.Add(PasteItem);
                    }
                    contextMenu.Show(this, e.Location);
                }
            }
        }
        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            if (!Editable || !_exePaint) return;

            int index = _layoutModel.GetIndexFromLocation(e.Location, _mainScrollBar.Value);
            if (index >= 0 && index < _dataModel.ItemCount())
            {
                //switch (_selectionMode)
                //{
                //  case SelectionMode.One:
                //    _selectedIndex = -1;
                //    break;
                //  case SelectionMode.MultiSimple:
                //  case SelectionMode.MultiExtended:
                //    SelectedIndices.Clear();
                //    SelectedItems.Clear();
                //    break;
                //}
                //Invalidate();
                //EnableEditAt(index);
                StartEdit();
            }
        }
        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            DrawBackground(pevent.Graphics);
            //base.OnPaintBackground(pevent);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            if (!Visible) return;
            Graphics graphics = e.Graphics;
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            //draw background and border
            //graphics.DrawRectangle(Pens.Gray, ClientRectangle);
            ////DrawBackground(graphics);
            graphics.SetClip(MainRect);
            //draw items
            // select item back color
            using (var sibc = new SolidBrush(SystemColors.Highlight))
            {
                //using (var sf = new StringFormat { Alignment = StringAlignment.Near, LineAlignment = StringAlignment.Center })
                //{
                //using when text long with horizontal scroll bar
                int transformX = !_multiColumn && _hScrollBar.Visible ? (_hScrollBar.Value) : 0;
                int transformY = 0;
                if (_isTrans)
                {
                    if (_multiColumn)
                    {
                        transformX = _transValue;
                    }
                    else
                    {
                        transformY = _transValue;
                    }
                }
                int firstIndex = _mainScrollBar.Value;
                if (_isTrans)
                {
                    firstIndex = _oldScrollVal;
                }
                //if (_multiColumn)
                //{
                //    int colSize = _layoutModel.GetNumItemBound(MainRect.Width);
                //    firstIndex /= colSize;
                //}
                if (Items.Count != 0)
                {
                    TextFormatFlags flags;
                    if (_multiColumn)
                    {
                        flags = TextFormatFlags.NoPadding;
                    }
                    else
                    {
                        flags = TextFormatFlags.VerticalCenter;
                    }
                    if (_ellipsisEnabled)
                    {
                        //sf.Trimming = StringTrimming.EllipsisCharacter;
                        flags |= TextFormatFlags.EndEllipsis;
                    }
                    if (UseTabStops)
                    {
                        flags |= TextFormatFlags.ExpandTabs;
                    }
                    if (RightToLeft == RightToLeft.Yes)
                    {
                        //sf.FormatFlags = StringFormatFlags.DirectionRightToLeft;
                        flags |= TextFormatFlags.Right;
                    }
                    int numItemLayout = _layoutModel.Count();
                    for (var i = 0; i < numItemLayout; i++)
                    {
                        int index = i + firstIndex;

                        if (index >= _dataModel.ItemCount()) return;

                        string itemText = getTextDisplay(_dataModel.Get(index));
                        Rectangle itemRect = _layoutModel.GetRect(i, firstIndex< _mainScrollBar.Value, transformX, transformY);
                        bool selected = false;
                        if ((int)_selectionMode > 1)
                        {
                            if (SelectedIndices.Contains(index))
                            {
                                selected = true;
                            }
                        }
                        else if ((int)_selectionMode == 1 && _selectedIndex == index)
                        {
                            selected = true;
                        }
                        if (DrawMode != DrawMode.Normal)
                        {
                            if (DrawItem != null)
                            {
                                _exePaint = true;
                                DrawItem?.Invoke(this, EventArgs.Empty);
                            }
                            else
                            {
                                _exePaint = false;
                            }
                        }
                        else
                        {
                            _exePaint = true;
                            //graphics.DrawString(itemText, Font, usic, itemRect, sf);
                            if (selected)
                            {
                                graphics.FillRectangle(sibc, itemRect);
                                TextRenderer.DrawText(graphics, itemText, Font, itemRect, Color.White, flags);
                            }
                            else
                            {
                                TextRenderer.DrawText(graphics, itemText, Font, itemRect, Enabled ? ForeColor : Color.Gray, flags);
                            }
                        }
                        if (_exePaint && Focused && !_internFocus && index == _focusIndex)
                        {
                            using (Pen dotGrayPen = new Pen(Color.Silver) { DashStyle = DashStyle.Dot })
                            {
                                if (SelectedIndex != index)
                                {
                                    dotGrayPen.Color = Color.DarkSlateGray;
                                }
                                //graphics.DrawRectangle(dotGrayPen, itemRect.X, itemRect.Y, itemRect.Width - 1, itemRect.Height - 1);
                                Helper.DrawRoundedRectangle(graphics, dotGrayPen, itemRect.X, itemRect.Y, itemRect.Width - 1, itemRect.Height - 1, 1, 1);
                            }
                        }
                    }

                }
                else if (DesignMode && DrawMode == DrawMode.Normal)
                {
                    //sf.LineAlignment = StringAlignment.Near;
                    TextRenderer.DrawText(graphics, Name, Font, ClientRectangle, Enabled ? ForeColor : Color.Gray, TextFormatFlags.Default);
                }
                //}
            }
            graphics.ResetClip();
        }

        protected override void OnGotFocus(EventArgs e)
        {
            if (!_internFocus)
            {
                int firstIndex = _mainScrollBar.Value;
                //if (_multiColumn)
                //{
                //    int colSize = _layoutModel.GetNumItemBound(MainRect.Width);
                //    firstIndex /= colSize;
                //}
                int rectIndex = _focusIndex - firstIndex;
                if (rectIndex >= 0 && rectIndex < _layoutModel.Count()) Invalidate(_layoutModel.GetRect(rectIndex));
            }
            base.OnGotFocus(e);
        }
        protected override void OnLostFocus(EventArgs e)
        {
            _internFocus = true;
            int firstIndex = _mainScrollBar.Value;
            //if (_multiColumn)
            //{
            //    int colSize = _layoutModel.GetNumItemBound(MainRect.Width);
            //    firstIndex /= colSize;
            //}
            int rectIndex = _focusIndex - firstIndex;
            if (rectIndex >= 0 && rectIndex < _layoutModel.Count()) Invalidate(_layoutModel.GetRect(rectIndex));
            _internFocus = false;
            base.OnLostFocus(e);
        }
        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            if (Visible)
            {
                InvalidateScroll(false);
            }
        }
        protected override void OnDragDrop(DragEventArgs e)
        {
            _indicator.Visible = false;
            int moveIndex = Math.Min(_indicator.Index - 1, _dataModel.ItemCount() - 1);
            if (_dragIndex != moveIndex)
            {
                //MoveItem(_dragIndex, moveIndex);
                //SelectedIndex = moveIndex;

                ////ItemMoveHistory undoItem = new ItemMoveHistory { LastIndex = _dragIndex, Data = SelectedItem };
                ////UndoRedoManager.Instance().AddToUndoStack(undoItem);
                //_indexSelectStack.Remove(moveIndex);
                //_indexSelectStack.AddLast(moveIndex);

                ItemMoveAction command = new ItemMoveAction(this, moveIndex, _dataModel.Get(_dragIndex));
                _undoRedoMng.PerformAction(command);
            }
            base.OnDragDrop(e);
        }
        protected override void OnResize(EventArgs e)
        {
            //base.OnResize(e);
            MainRect = Rectangle.Inflate(ClientRectangle, -(int)_borderStyle, -(int)_borderStyle);
            //_lastHscrollVisible = _lastVscrollVisible = false;
            if (_lastHscrollVisible)
            {
                InvalidMainH(true);
            }
            if (_lastVscrollVisible)
            {
                InvalidMainV(true);
            }
            if (_multiColumn)
            {
                InvalidateHScrollRect();
            }
            else
            {
                InvalidateVScrollRect();
                if (_horizontalScrollBar) InvalidateHScrollRect();
            }
            InvalidateScroll(true);
        }
        protected override void OnDragEnter(DragEventArgs e)
        {
            if (Items.Count > 1 && e.Data.GetDataPresent(Items[0].GetType()))
            {
                _indicator.Visible = true;
                e.Effect = e.AllowedEffect;
                OnDragOver(e);
            }
            base.OnDragEnter(e);
        }
        protected override void OnDragLeave(EventArgs e)
        {
            _indicator.Visible = false;
            base.OnDragLeave(e);
        }
        protected override void OnDragOver(DragEventArgs e)
        {
            base.OnDragOver(e);
            if (!AllowDrop) return;

            Point screenCoords = Cursor.Position;
            Point controlPoint = PointToClient(screenCoords);
            int pageSize;
            int firstIndex = _mainScrollBar.Value;
            if (_multiColumn && _columnWidth != 0)
            {
                //firstIndex /= pageSize;
                //if (_columnWidth != 0)
                //{
                pageSize = _layoutModel.ColumnSize;
                //}

            }
            else
            {
                pageSize = _layoutModel.PageSize;
            }
            int index = _layoutModel.GetIndexBeyondFromLocation(controlPoint, firstIndex);

            int lastVisbileIndex = Math.Min(firstIndex + pageSize, Items.Count);
            //if (index < 0) index = lastVisbileIndex;
            if (!_exeScroll)
            {
                ExecuteVisibleNextScroll(index, controlPoint, firstIndex, lastVisbileIndex);
            }
            if (index >= _dataModel.ItemCount() || index < 0)
            {
                e.Effect = DragDropEffects.None;
                return;
            }
            else
            {
                e.Effect = DragDropEffects.Move;
            }
            _indicator.Index = index + 1;
            if (!_multiColumn)
            {
                _indicator.Width = MainRect.Width;
                _indicator.Location = new Point(MainRect.X, controlPoint.Y);
            }
            else
            {
                Rectangle cachedRect = _layoutModel.GetCachedRect(index - firstIndex);
                _indicator.Width = cachedRect.Width;
                _indicator.Location = new Point(cachedRect.X, controlPoint.Y);
            }
            _indicator.Repaint();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            _dragDropTimer.Stop();
            _indicator.Visible = false;
        }
        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (!_exePaint) return;
            if (_selectionMode == SelectionMode.One && e.Button == MouseButtons.Left && !Sorted)
            {
                if (_dragDropTimer.Enabled)
                {
                    _dragDropTimer.Stop();
                }
                _dragDropTimer.Start();
                //OnMouseClick(e);
            }
            _mouseDownPoint = e.Location;
            base.OnMouseDown(e);
        }
        #endregion
        #endregion


    }
}
