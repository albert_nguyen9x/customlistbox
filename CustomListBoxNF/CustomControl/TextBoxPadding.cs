﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomListBoxNF.CustomControl
{
    public partial class TextBoxPadding : UserControl
    {
        public TextBoxPadding(Padding padding)
        {
            InitializeComponent();
            Padding = padding;
            textBox1.KeyDown += TextBoxKeyDown;
        }

        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            OnKeyDown(e);
        }

        public TextBoxPadding(int left = 0, int top = 0, int right = 0, int bottom =0)
        {
            InitializeComponent();
            Padding = new Padding(left, top, right, bottom);
            textBox1.KeyDown += TextBoxKeyDown;
        }
        public override string Text { get => textBox1.Text; set => textBox1.Text = value; }
        public int SelectionStart { get => textBox1.SelectionStart; set => textBox1.SelectionStart = value; }
        public int SelectionLength { get => textBox1.SelectionLength; set => textBox1.SelectionLength = value; }
    }
}
