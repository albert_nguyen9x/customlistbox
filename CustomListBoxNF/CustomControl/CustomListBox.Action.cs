﻿using CustomListBoxNF.Actions;
using CustomListBoxNF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomListBoxNF.CustomControl
{
    partial class CustomListBox
    {
        internal bool StartEdit()
        {
            if (!Editable || EditMode || _dataModel.ItemCount()==0)
            {
                return false;
            }
            if(_cachedPropertyDescs?.Count >= 1 && _displayMember=="")
            {
                object data = _dataModel.Get(0);
                if(data.GetType() != typeof(string))
                {
                    return false;
                }
            }
            if (_indexSelectStack.Count != 0)
            {
                EnableEditAt(_indexSelectStack.Last.Value);
                return true;
            }
            else if (SelectedIndices.Count != 0)
            {
                EnableEditAt(SelectedIndices[0]);
                return true;
            }
            return false;
        }
        internal bool FinishEdit()
        {
            if (!Editable || !EditMode)
            {
                return false;
            }
            EditMode = false;
            object data = _textBox.Text;
            if (_dataSource!=null && _enableFormating && _formatString != "")
            {
                object originval;
                if (_formater.TryConvertBack(_textBox.Text, out originval))
                {
                    data = originval;
                }
            }
            ItemEditorAction command = new ItemEditorAction(this, _editIndex, data);
            return _undoRedoMng.PerformAction(command);

        }
        internal bool CancelEdit()
        {
            if (!Editable || !EditMode)
            {
                return false;
            }
            EditMode = false;
            return true;
        }
        internal bool Undo()
        {
            if (_undoRedoMng.CanUndo)
            {
                return _undoRedoMng.Undo();
            }
            return false;
        }
        internal bool Redo()
        {
            if (_undoRedoMng.CanRedo)
            {
                return _undoRedoMng.Redo();
            }
            return false;
        }
        private int getLastSelectIndex(bool shiftPress)
        {
            if (_indexSelectStack.Count != 0)
            {
                if (!shiftPress)
                {
                    return _indexSelectStack.Last.Value;
                }
                else
                {
                    if (_lastIndexSel != -1)
                    {
                        removeRangeLastSelection();
                        return _lastIndexSel;
                    }
                    else
                    {
                        return _indexSelectStack.Last.Value;
                    }
                }
            }
            return -1;
        }
        private void addSelected(int moveIndex, bool shiftPress)
        {
            if (!shiftPress)
            {
                if (_indexSelectStack.Count != 0)
                {
                    _indexSelectStack.RemoveLast();
                }
                _indexSelectStack.AddLast(moveIndex);
                _focusIndex = moveIndex;
                _lastIndexSel = -1;
                if (_selectionMode == SelectionMode.One)
                {
                    SelectedItem = Items[moveIndex];
                    SelectedIndex = moveIndex;
                    if (_dataBindingManager != null && _valueMember != "")
                    {
                        SelectedValue = Helper.GetPropertyValueFromDataSource(_valueMember, _cachedPropertyDescs, Items[moveIndex]).ToString();
                    }
                }
                else
                {
                    SelectedIndices.Clear();
                    SelectedItems.Clear();
                    SelectedIndices.Add(moveIndex);
                    SelectedItems.Add(Items[moveIndex]);
                }

            }
            else
            {
                _lastIndexSel = moveIndex;
                addRangeSelection();
            }
            EnsureVisible(moveIndex);
        }
        internal bool SelectPrevCol(bool shiftPress)
        {
            if (_dataModel.ItemCount() == 0 || !_exePaint || _selectionMode == SelectionMode.None
                || _selectionMode == SelectionMode.MultiSimple)
            {
                return false;
            }
            shiftPress &= _selectionMode == SelectionMode.MultiExtended;

            int moveIndex = 0;
            int index = getLastSelectIndex(shiftPress);
            bool rTL = RightToLeft == RightToLeft.Yes;
            if (index != -1)
            {
                if (_multiColumn)
                {
                    if (rTL)
                    {
                        moveIndex = index + _layoutModel.ColumnSize;
                        if (moveIndex >= _dataModel.ItemCount())
                        {
                            if (_dataModel.ItemCount() % _layoutModel.ColumnSize != 0 && (index/ _layoutModel.ColumnSize == _dataModel.ItemCount() / _layoutModel.ColumnSize))
                            {
                                return false;
                            }
                            moveIndex = _dataModel.ItemCount() - 1;
                        }
                    }
                    else
                    {
                        moveIndex = index - _layoutModel.ColumnSize;
                    }
                    if (moveIndex < 0) return false;
                }
                else
                {
                    moveIndex = Math.Max(0, index - 1);
                }
            }
            if (moveIndex == index)
            {
                return false;
            }
            addSelected(moveIndex, shiftPress);
            return true;
        }
        internal bool SelectNextCol(bool shiftPress)
        {
            if (_dataModel.ItemCount() == 0 || !_exePaint || _selectionMode == SelectionMode.None
                || _selectionMode == SelectionMode.MultiSimple)
            {
                return false;
            }
            shiftPress &= _selectionMode == SelectionMode.MultiExtended;

            int moveIndex = 0;
            bool rTL = RightToLeft == RightToLeft.Yes;
            int index = getLastSelectIndex(shiftPress);
            if (index != -1)
            {
                if (_multiColumn)
                {
                    if (rTL)
                    {
                        moveIndex = index - _layoutModel.ColumnSize;
                        if (moveIndex < 0) return false;
                    }
                    else
                    {
                        moveIndex = index + _layoutModel.ColumnSize;
                    }
                    if(moveIndex>= _dataModel.ItemCount())
                    {
                        if (_dataModel.ItemCount() % _layoutModel.ColumnSize != 0 && (index / _layoutModel.ColumnSize == _dataModel.ItemCount() / _layoutModel.ColumnSize))
                        {
                            return false;
                        }
                        moveIndex = _dataModel.ItemCount() - 1;
                    }
                }
                else
                {
                    moveIndex = Math.Min(_dataModel.ItemCount() - 1, index + 1);
                }
            }
            if (moveIndex == index)
            {
                return false;
            }
            addSelected(moveIndex, shiftPress);
            return true;
        }
        internal bool SelectPrevRow(bool shiftPress)
        {
            if (_dataModel.ItemCount() == 0 || !_exePaint || _selectionMode == SelectionMode.None
                || _selectionMode == SelectionMode.MultiSimple)
            {
                return false;
            }
            shiftPress &= _selectionMode == SelectionMode.MultiExtended;

            int moveIndex = 0;
            int index = getLastSelectIndex(shiftPress);
            if (index != -1)
            {
                moveIndex = Math.Max(0, index - 1);
            }
            if (moveIndex == index)
            {
                return false;
            }
            addSelected(moveIndex, shiftPress);
            return true;
        }
        internal bool SelectNextRow(bool shiftPress)
        {
            if (_dataModel.ItemCount() == 0 || !_exePaint || _selectionMode == SelectionMode.None
                || _selectionMode == SelectionMode.MultiSimple)
            {
                return false;
            }
            shiftPress &= _selectionMode == SelectionMode.MultiExtended;

            int moveIndex = 0;
            int index = getLastSelectIndex(shiftPress);
            if (index != -1)
            {
                moveIndex = Math.Min(_dataModel.ItemCount() - 1, index + 1);
            }
            if (moveIndex == index)
            {
                return false;
            }
            addSelected(moveIndex, shiftPress);
            return true;
        }
        internal bool SelectFirst(bool shiftPress)
        {
            if (_dataModel.ItemCount() == 0 || !_exePaint || _selectionMode == SelectionMode.None
                || _selectionMode == SelectionMode.MultiSimple)
            {
                return false;
            }
            shiftPress &= _selectionMode == SelectionMode.MultiExtended;

            int moveIndex = 0;
            int index = getLastSelectIndex(shiftPress);
            if (moveIndex == index)
            {
                return false;
            }
            addSelected(moveIndex, shiftPress);
            return true;
        }
        internal bool SelectLast(bool shiftPress)
        {
            if (_dataModel.ItemCount() == 0 || !_exePaint || _selectionMode == SelectionMode.None
                || _selectionMode == SelectionMode.MultiSimple)
            {
                return false;
            }
            shiftPress &= _selectionMode == SelectionMode.MultiExtended;

            int moveIndex = _dataModel.ItemCount()-1;
            int index = getLastSelectIndex(shiftPress);
            if (moveIndex == index)
            {
                return false;
            }
            addSelected(moveIndex, shiftPress);
            return true;
        }
        internal bool SelectFirstPage(bool shiftPress)
        {
            if (_dataModel.ItemCount() == 0 || !_exePaint || _selectionMode == SelectionMode.None
                || _selectionMode == SelectionMode.MultiSimple)
            {
                return false;
            }
            shiftPress &= _selectionMode == SelectionMode.MultiExtended;

            int moveIndex = 0;
            int index = getLastSelectIndex(shiftPress);
            if (index != -1)
            {
               moveIndex = Math.Max(0, index - (_layoutModel.PageSize - 1));
            }
            if (moveIndex == index)
            {
                return false;
            }
            addSelected(moveIndex, shiftPress);
            return true;
        }
        internal bool SelectLastPage(bool shiftPress)
        {
            if (_dataModel.ItemCount() == 0 || !_exePaint || _selectionMode == SelectionMode.None
                || _selectionMode == SelectionMode.MultiSimple)
            {
                return false;
            }
            shiftPress &= _selectionMode == SelectionMode.MultiExtended;

            int moveIndex = 0;
            int index = getLastSelectIndex(shiftPress);
            if (index != -1)
            {
                moveIndex = Math.Min(_dataModel.ItemCount() - 1, index + (_layoutModel.PageSize - 1));
            }
            if (moveIndex == index)
            {
                return false;
            }
            addSelected(moveIndex, shiftPress);
            return true;
        }
    }
}
