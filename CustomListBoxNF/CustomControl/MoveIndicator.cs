﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomListBoxNF.CustomControl
{
  [ToolboxItem(false)]
  public partial class MoveIndicator : Control
  {
    private readonly int Radius = 8;
    private Point _indicator = new Point(8, 8);
    public override string Text
    {
      get => base.Text;
      set
      {
        if (base.Text != value)
        {
          base.Text = value;
        }
      }
    }
    private int _index;

    public int Index
    {
      get { return _index; }
      set
      {
        if (_index != value) _index = value;

      }
    }

    public new Size Size
    {
      get => base.Size;
      set
      {
        if (value != Size)
        {
          base.Size = value;
          _indicator = new Point(8, Height / 2);
        }
      }
    }
    public MoveIndicator()
    {
      SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.OptimizedDoubleBuffer, true);
      UpdateStyles();
      InitializeComponent();
      Text = "1";
      if(Parent!=null) Font = Parent.Font;
    }

    public void Repaint()
    {
      Invalidate();
    }

    protected override void OnPaint(PaintEventArgs pe)
    {
      Graphics graphics = pe.Graphics;
      if (Height > 16 && Width > 16)
      {
        graphics.FillEllipse(Brushes.White,
             _indicator.X - Radius, _indicator.Y - Radius, 2 * Radius, 2 * Radius);
        graphics.DrawEllipse(Pens.DarkRed,
           _indicator.X - Radius, _indicator.Y - Radius, 2 * Radius, 2 * Radius);
      }
      
        using (StringFormat label_format = new StringFormat())
        {
          label_format.Alignment = StringAlignment.Center;
          label_format.LineAlignment = StringAlignment.Center;
          graphics.DrawString(Index+"", Font, Brushes.Black, _indicator, label_format);
          label_format.Alignment = StringAlignment.Near;
          graphics.DrawString(Text, Font, Brushes.Black, _indicator.X + Radius + 1, _indicator.Y, label_format);
        }
    }
  }
}
