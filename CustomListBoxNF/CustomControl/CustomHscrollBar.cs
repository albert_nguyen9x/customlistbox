﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomListBoxNF.CustomControl
{
    [ToolboxItem(false)]
    internal class CustomHscrollBar : HScrollBar
    {
        private CustomListBox _parent;
        public CustomHscrollBar(CustomListBox parent)
        {
            _parent = parent;
        }
        protected override void WndProc(ref Message m)
        {
            //base.WndProc(ref m);
            switch (m.Msg)
            {
                //WM_SIZE
                case 0x0005:
                    int stX = (int)_parent.BorderStyle;
                    if (Parent != null)
                    {
                        Point newLocation = new Point(stX, _parent.Height - SystemInformation.HorizontalScrollBarHeight - stX);
                        int width = Parent.Width - 2 * stX;
                        if (_parent.Width - _parent.MainRect.Width > 4)
                        {
                            if (_parent.RightToLeft == RightToLeft.Yes)
                            {
                                newLocation = new Point(stX + SystemInformation.VerticalScrollBarWidth,
                                    _parent.Height - SystemInformation.HorizontalScrollBarHeight - stX);
                            }
                            width -= SystemInformation.VerticalScrollBarWidth;
                        }
                        Size newSize = new Size(width, SystemInformation.HorizontalScrollBarHeight);
                        if (Size != newSize)
                        {
                            Size = newSize;
                        }

                        if (Location != newLocation)
                        {
                            Location = newLocation;
                        }
                    }
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        internal void EnableChanged(bool enable)
        {
            SetStyle(ControlStyles.Selectable, enable);
            UpdateStyles();
        }
    }
}
