﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;

namespace CustomListBoxNF.CustomControl
{
    [ToolboxItem(false)]
    internal class CustomVscrollbar : VScrollBar
    {
        private CustomListBox _parent;
        public CustomVscrollbar(CustomListBox parent)
        {
            _parent = parent;
        }
        protected override void WndProc(ref Message m)
        {
            //base.WndProc(ref m);
            switch (m.Msg)
            {
                //WM_SIZE
                case 0x0005:
                    int stX = (int)_parent.BorderStyle;
                    if (Parent != null)
                    {
                        int height = Parent.Height - 2 * stX;
                        if (_parent.Height - _parent.MainRect.Height > 4)
                        {
                            height -= SystemInformation.VerticalScrollBarWidth;
                        }
                        Size newSize = new Size(SystemInformation.VerticalScrollBarWidth, height);
                        if (Size != newSize)
                        {
                            Size = newSize;
                        }
                        Point newLocation;
                        if (_parent.RightToLeft == RightToLeft.Yes)
                        {
                            newLocation = new Point(stX, stX);

                        }
                        else
                        {
                            newLocation = new Point(Parent.Width - SystemInformation.VerticalScrollBarWidth - stX, stX);
                        }
                        if (Location != newLocation)
                        {
                            Location = newLocation;
                        }
                    }
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        internal void EnableChanged(bool enable)
        {
            SetStyle(ControlStyles.Selectable, enable);
            UpdateStyles();
        }
    }
}
