﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Attributes
{
  internal class LBDescriptionAttr: DescriptionAttribute
  {

    public LBDescriptionAttr(string resName)
    {
      DescriptionValue = Properties.Resources.ResourceManager.GetString(resName);
    }
  }
}
