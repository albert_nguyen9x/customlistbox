﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomListBoxNF.Utils
{
    internal class Helper
    {

        public static object GetPropertyValueFromDataSource(string property, PropertyDescriptorCollection propertyDescriptor, object data)
        {
            PropertyDescriptor ret = propertyDescriptor.Find(property, false);
            return ret.GetValue(data);
        }
        public static bool SetPropertyValueFromDataSource(string property, PropertyDescriptorCollection propertyDescriptor, object data, object newval)
        {
            PropertyDescriptor ret = propertyDescriptor.Find(property, false);
            if (ret == null)
            {
                return false;
            }
            if (newval.GetType() != ret.PropertyType)
            {
                newval = Convert.ChangeType(newval, ret.PropertyType);
            }
            ret.SetValue(data, newval);
            return true;
        }
        public static bool CheckPropertyValid(string property, PropertyDescriptorCollection propertyDescriptor) => propertyDescriptor.Find(property, false) != null;

        public static string GetAttributeString<T>(object source, string sourceProperty, string attributeProperty)
        {     // Get attribute adorning the specified property of a particular component instance
            PropertyInfo sourcePropertyInfo = source.GetType().GetProperty(sourceProperty);
            T attribute = (T)sourcePropertyInfo.GetCustomAttributes(typeof(T), false)[0];
            if (attribute == null) return null;
            // Return the desired attribute's property value
            Type attributeType = attribute.GetType();
            PropertyInfo attributePropertyInfo = attributeType.GetProperty(attributeProperty);
            return (string)attributePropertyInfo.GetValue(attribute, null);
        }
        public static void DrawRoundedRectangle(Graphics g, Pen p, int x, int y, int w, int h, int rx, int ry)
        {
            GraphicsPath path = new GraphicsPath();
            path.AddArc(x, y, rx + rx, ry + ry, 180, 90);
            path.AddLine(x + rx, y, x + w - rx, y);
            path.AddArc(x + w - 2 * rx, y, 2 * rx, 2 * ry, 270, 90);
            path.AddLine(x + w, y + ry, x + w, y + h - ry);
            path.AddArc(x + w - 2 * rx, y + h - 2 * ry, rx + rx, ry + ry, 0, 91);
            path.AddLine(x + rx, y + h, x + w - rx, y + h);
            path.AddArc(x, y + h - 2 * ry, 2 * rx, 2 * ry, 90, 91);
            path.CloseFigure();
            g.DrawPath(p, path);
        }
    }
}
