﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Utils
{
    internal interface IFormatData
    {
        bool TryConvertToString(object data, out string convertData);
        bool TryConvertBack(string data, out object originData);
    }
    internal class FormatContainer
    {
        public static IFormatData Default = new DefaultFormat();
        private static NumberFormat _numberFormat = new NumberFormat();
        private static DateTimeFormat _datetimeFormat = new DateTimeFormat();
        private static PercentFormat _percentFormat = new PercentFormat();

        public static IFormatData GetFormaterFromString(string formatString)
        {
            FormatType type = getFormatType(formatString);
            switch (type)
            {
                case FormatType.Number:
                    if(formatString[0] == 'P')
                    {
                        _percentFormat.FormatString = formatString;
                        return _percentFormat;
                    }
                    else
                    {
                        _numberFormat.FormatString = formatString;
                        return _numberFormat;
                    }
                case FormatType.Datetime:
                    _datetimeFormat.FormatString = formatString;
                    return _datetimeFormat;
                default:
                    return Default;
            }
        }

        private static FormatType getFormatType(string formatString)
        {
            switch (formatString[0])
            {
                case 'C':
                case 'P':
                case 'N':
                case 'E':
                    return FormatType.Number;
                default:
                    return FormatType.Datetime;
            }
        }
    }
    internal class DefaultFormat : IFormatData
    {
        public bool TryConvertBack(string data, out object originData)
        {
            originData = new object();
            originData = data;
            return true;
        }

        public bool TryConvertToString(object data, out string convertData)
        {
            convertData = data.ToString();
            return true;
        }
    }
    internal class NumberFormat : IFormatData
    {
        public string FormatString { get; set; }

        public bool TryConvertBack(string data, out object originData)
        {
            originData = new object();
            try
            {
                originData = decimal.Parse(data, NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint | NumberStyles.Currency);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool TryConvertToString(object data, out string convertData)
        {
            convertData = "";
            if (data is IFormattable format)
            {
                try
                {
                    convertData = format.ToString(FormatString, null);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
    internal class PercentFormat : IFormatData
    {
        public string FormatString { get; set; }

        public bool TryConvertBack(string data, out object originData)
        {
            originData = new object();
            try
            {
                originData = decimal.Parse(data.Substring(0, data.Length - 1))/100;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool TryConvertToString(object data, out string convertData)
        {
            convertData = "";
            if (data is IFormattable format)
            {
                try
                {
                    convertData = format.ToString(FormatString, null);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
    internal class DateTimeFormat : IFormatData
    {
        public string FormatString { get; set; }

        public bool TryConvertBack(string data, out object originData)
        {
            originData = new object();
            try
            {
                originData = DateTime.Parse(data);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool TryConvertToString(object data, out string convertData)
        {
            convertData = "";
            if (data is IFormattable format)
            {
                try
                {
                    convertData = format.ToString(FormatString, null);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
    enum FormatType
    {
        Number,
        Datetime
    }
}
