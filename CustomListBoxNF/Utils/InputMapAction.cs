﻿using CustomListBoxNF.Actions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace CustomListBoxNF.Utils
{
    internal class InputMapAction
    {
        Dictionary<KeyStroke, Action> _dataMap = new Dictionary<KeyStroke, Action>();
        private static InputMapAction mapWhenFocus = CreateFocusMap();
        private static InputMapAction mapWhenEditorFocus = CreateEditorFocusMap();

        private static InputMapAction CreateEditorFocusMap()
        {
            InputMapAction mapAction = new InputMapAction();
            mapAction.Put(new KeyStroke(Keys.Enter, Keys.None), new FinishEditAction());
            mapAction.Put(new KeyStroke(Keys.Escape, Keys.None), new CancelEditAction());
            return mapAction;
        }

        private static InputMapAction CreateFocusMap()
        {
            InputMapAction mapAction = new InputMapAction();
            mapAction.Put(new KeyStroke(Keys.F2, Keys.None), new StartEditAction());
            mapAction.Put(new KeyStroke(Keys.Left, Keys.None), new SelectPrevColAction(shiftPress:false));
            mapAction.Put(new KeyStroke(Keys.Left, Keys.Shift), new SelectPrevColAction(shiftPress: true));
            mapAction.Put(new KeyStroke(Keys.Right, Keys.None), new SelectNextColAction(shiftPress: false));
            mapAction.Put(new KeyStroke(Keys.Right, Keys.Shift), new SelectNextColAction(shiftPress: true));
            mapAction.Put(new KeyStroke(Keys.Up, Keys.None), new SelectPrevRowAction(shiftPress: false));
            mapAction.Put(new KeyStroke(Keys.Up, Keys.Shift), new SelectPrevRowAction(shiftPress: true));
            mapAction.Put(new KeyStroke(Keys.Down, Keys.None), new SelectNextRowAction(shiftPress: false));
            mapAction.Put(new KeyStroke(Keys.Down, Keys.Shift), new SelectNextRowAction(shiftPress: true));
            mapAction.Put(new KeyStroke(Keys.Home, Keys.None), new SelectFirstAction(shiftPress: false));
            mapAction.Put(new KeyStroke(Keys.Home, Keys.Shift), new SelectFirstAction(shiftPress: true));
            mapAction.Put(new KeyStroke(Keys.End, Keys.None), new SelectLastAction(shiftPress: false));
            mapAction.Put(new KeyStroke(Keys.End, Keys.Shift), new SelectLastAction(shiftPress: true));
            mapAction.Put(new KeyStroke(Keys.PageUp, Keys.None), new SelectFirstPageAction(shiftPress: false));
            mapAction.Put(new KeyStroke(Keys.PageUp, Keys.Shift), new SelectFirstPageAction(shiftPress: true));
            mapAction.Put(new KeyStroke(Keys.PageDown, Keys.None), new SelectLastPageAction(shiftPress: false));
            mapAction.Put(new KeyStroke(Keys.PageDown, Keys.Shift), new SelectLastPageAction(shiftPress: true));
            mapAction.Put(new KeyStroke(Keys.Z, Keys.Control), new ListBoxUndoAction());
            mapAction.Put(new KeyStroke(Keys.Y, Keys.Control), new ListBoxRedoAction());

            return mapAction;
        }

        public static InputMapAction GetMap(InputMapType inputMapType)
        {
            switch (inputMapType)
            {
                case InputMapType.Focus:
                    return mapWhenFocus;
                case InputMapType.EditorFocus:
                    return mapWhenEditorFocus;
                default:
                    return null;
            }
        }
        public Action GetAction(KeyStroke key)
        {
            Action action;
            bool success = _dataMap.TryGetValue(key, out action);
            if (success) return action;
            return null;
        }
        private void Put(KeyStroke key, Action action)
        {
            _dataMap.Add(key, action);
        }
    }
    internal enum InputMapType
    {
        Focus,
        EditorFocus
    }
    internal struct KeyStroke
    {
        public Keys KeyCode { get; set; }
        public Keys ModifierKey { get; set; }
        public KeyStroke(Keys keyCode, Keys modifierKey)
        {
            KeyCode = keyCode;
            ModifierKey = modifierKey;
        }
        public static bool operator ==(KeyStroke k1, KeyStroke k2)
        {
            return k1.KeyCode == k2.KeyCode && k1.ModifierKey == k2.ModifierKey;
        }
        public static bool operator !=(KeyStroke k1, KeyStroke k2)
        {
            return !(k1 == k2);
        }

        public override bool Equals(object obj)
        {
            if(obj is KeyStroke keyStroke) { 
                return this == keyStroke;
            }
            return false;
        }
        public override int GetHashCode()
        {
            return KeyCode.GetHashCode() & Keys.Modifiers.GetHashCode();
        }
    }
}
