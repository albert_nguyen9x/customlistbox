﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Utils
{
  #region define class Items Collection
  public class ObjectList : ArrayList
  {
    public override int Add(object value)
    {
      int res = base.Add(value);
      if (res >= 0) ItemListChanged?.Invoke(this, EventArgs.Empty);
      return res;
    }
    public override void AddRange(ICollection c)
    {
      base.AddRange(c);
      ItemListChanged?.Invoke(this, EventArgs.Empty);
    }
    public override void Remove(object obj)
    {
      base.Remove(obj);
      ItemListChanged?.Invoke(this, EventArgs.Empty);
    }
    public override void RemoveAt(int index)
    {
      base.RemoveAt(index);
      ItemListChanged?.Invoke(this, EventArgs.Empty);
    }
    public override void InsertRange(int index, ICollection c)
    {
      base.InsertRange(index, c);
      ItemListChanged?.Invoke(this, EventArgs.Empty);
    }
    public override void Clear()
    {
      base.Clear();
      ItemListChanged?.Invoke(this, EventArgs.Empty);
    }
    public override void RemoveRange(int index, int count)
    {
      base.RemoveRange(index, count);
      ItemListChanged?.Invoke(this, EventArgs.Empty);
    }
    public event EventHandler ItemListChanged;


  }
  #endregion
}
