﻿using CustomListBoxNF.CustomControl;
using CustomListBoxNF.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomListBoxNF.Model
{
    internal class DataModel
    {
        private CustomListBox _owner;
        internal ObjectList Items;
        public int ItemLongest { get; private set; } = 0;
        public DataModel(CustomListBox owner)
        {
            _owner = owner;
            Items = new ObjectList();
            Items.ItemListChanged += OnItemChanged;
        }

        private void OnItemChanged(object sender, EventArgs e)
        {
            ItemLongest = 0;
            foreach (object o in Items)
            {
                ItemLongest = Math.Max(ItemLongest, TextRenderer.MeasureText(_owner.getTextDisplay(o), _owner.Font).Width);
            }
            _owner.InvalidateScroll(true);
        }
        public void Sort() => Items.Sort();
        public int IndexOf(object o) => Items.IndexOf(o);
        public void Reset()
        {
            Items.Clear();
        }
        public object Get(int index)
        {
            if (index >= Items.Count) throw new IndexOutOfRangeException();
            return Items[index];
        }
        public void Add(object o) => Items.Add(o);
        public void Replace(int index, object o) => Items[index] = o;
        public void RemoveAt(int index)
        {
            if (index >= Items.Count) throw new IndexOutOfRangeException();
            Items.RemoveAt(index);
        }
        public void Remove(object o)
        {
            Items.Remove(o);
        }
        public void AddRange(ICollection collection) => Items.AddRange(collection);
        public int ItemCount() => Items.Count;
        public void MoveItem(int currentIndex, int targetIndex)
        {
            object moveItem = Items[currentIndex];
            object[] replace = new object[Items.Count];
            Items.RemoveAt(currentIndex);
            object[] arrData = Items.ToArray();
            if (targetIndex != 0)
            {
                Array.Copy(arrData, 0, replace, 0, targetIndex);
            }
            replace[targetIndex] = moveItem;
            if (targetIndex != arrData.Length)
            {
                Array.Copy(arrData, targetIndex, replace, targetIndex + 1, arrData.Length - targetIndex);
            }
            Items.Clear();
            Items.AddRange(replace);
        }

        internal void OnBindingDataChanged(ListChangedEventArgs e, CurrencyManager dataBindingManager)
        {
            if (e.ListChangedType == ListChangedType.Reset || e.ListChangedType == ListChangedType.ItemMoved)
            {
                RefreshBindingData(dataBindingManager);
            }
            else if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                Add(dataBindingManager.List[e.NewIndex]);
            }
            else if (e.ListChangedType == ListChangedType.ItemChanged)
            {
                Replace(e.NewIndex, dataBindingManager.List[e.NewIndex]);
            }
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
            {
                RemoveAt(e.NewIndex);
            }
            else
            {
                RefreshBindingData(dataBindingManager);
            }
        }

        internal void RefreshBindingData(CurrencyManager dataBindingManager)
        {
            Reset();
            object[] data = new object[dataBindingManager.Count];
            int index = 0;
            foreach (object o in dataBindingManager.List) data[index++] = o;
            AddRange(data);
        }
    }
}
