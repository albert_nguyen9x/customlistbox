﻿using CustomListBoxNF.CustomControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomListBoxNF.Model
{
    internal class LayoutModel
    {
        public int ColumnSize { get; set; } = 0;
        public int ColumnWidth { get; set; } = 0;
        public int PageSize { get; set; } = 0;
        private List<Rectangle> _recItemsCached = new List<Rectangle>();
        private CustomListBox _owner { get; set; }

        public LayoutModel(CustomListBox owner)
        {
            _owner = owner;
        }
        public int Count() => _recItemsCached.Count;

        public Rectangle GetCachedRect(int index) => _recItemsCached[index];

        public void ResetCached()
        {
            _recItemsCached.Clear();
        }

        public void ReCached(int numItem, int itemHeight, int itemWidth)
        {
            ResetCached();
            Rectangle OwnerBounds = _owner.MainRect;
            bool rToL = _owner.RightToLeft == RightToLeft.Yes;
            if (!_owner.MultiColumn)
            {
                PageSize = OwnerBounds.Height / itemHeight;
            }
            else
            {
                ColumnSize = OwnerBounds.Height / itemHeight;
                ColumnWidth = itemWidth;
                PageSize = ColumnSize * (OwnerBounds.Width / itemWidth);
            }
            int x = OwnerBounds.X, y = OwnerBounds.Y;
            if (rToL)
            {
                x += OwnerBounds.Width - 1;
            }
            for (int i = 0; i < numItem; i++)
            {
                if (!_owner.MultiColumn)
                {
                    int xCheck = x;
                    if (rToL)
                    {
                        xCheck -= itemHeight / 3;
                    }
                    else
                    {
                        xCheck += itemHeight / 3;
                    }
                    if (!OwnerBounds.Contains(xCheck, y))
                    {
                        return;
                    }
                }
                else
                {
                    if (!OwnerBounds.Contains(x, y + itemHeight - 1))
                    {
                        //if (!_owner.MultiColumn)
                        //{
                        //    return;
                        //}
                        if (rToL)
                        {
                            x -= itemWidth;
                        }
                        else
                        {
                            x += itemWidth;
                        }
                        y = OwnerBounds.Y;
                        if (!OwnerBounds.Contains(x, y))
                        {
                            return;
                        }
                    }
                }
                if (!rToL)
                {
                    _recItemsCached.Add(new Rectangle(x, y, itemWidth, itemHeight));
                }
                else
                {
                    _recItemsCached.Add(new Rectangle(x - itemWidth + 1, y, itemWidth, itemHeight));
                }
                y += itemHeight;
            }

        }
        //public void GetCurrentBoundRange(int scrollVal, int itemWidth, out int firstIndex, out int numItem)
        //{
        //    Rectangle OwnerBounds = _owner.MainRect;
        //    firstIndex = numItem = 0;
        //    if (!_owner.MultiColumn)
        //    {
        //        numItem = OwnerBounds.Height / _owner.ItemHeight;
        //        firstIndex = scrollVal;
        //    }
        //    else
        //    {
        //        numItem = OwnerBounds.Height / _owner.ItemHeight * (OwnerBounds.Width / itemWidth);
        //        firstIndex = scrollVal * numItem;
        //    }
        //}
        //public int GetNumItemBound(int itemWidth)
        //{
        //    Rectangle OwnerBounds = _owner.MainRect;
        //    if (!_owner.MultiColumn)
        //    {
        //        return OwnerBounds.Height / _owner.ItemHeight;
        //    }
        //    else
        //    {
        //        return OwnerBounds.Height / _owner.ItemHeight * (OwnerBounds.Width / itemWidth);
        //    }
        //}
        public int GetIndexFromLocation(Point location, int firstIndex)
        {
            int index = -1;
            bool rTL = _owner.RightToLeft == RightToLeft.Yes;
            Rectangle OwnerBounds = _owner.MainRect;
            if (!_owner.MultiColumn)
            {
                index = location.Y / _owner.ItemHeight;
            }
            else
            {
                int x = location.X;
                if (rTL)
                {
                    x = OwnerBounds.X + OwnerBounds.Width - 1 - x;
                }
                index = (x / ColumnWidth) * (OwnerBounds.Height / _owner.ItemHeight) + (location.Y / _owner.ItemHeight);
                if (index < _recItemsCached.Count && !_recItemsCached[index].Contains(location))
                {
                    return -1;
                }
            }
            return firstIndex + index;
        }
        public int GetIndexBeyondFromLocation(Point location, int firstIndex)
        {
            int index = -1;
            Rectangle OwnerBounds = _owner.MainRect;
            if (!_owner.MultiColumn || (_owner.MultiColumn && _owner.ColumnWidth == 0))
            {
                index = firstIndex + location.Y / _owner.ItemHeight;
            }
            else
            {
                index = firstIndex + (location.X / _owner.ColumnWidth) * (OwnerBounds.Height / _owner.ItemHeight)
                  + (location.Y / _owner.ItemHeight);
            }
            return index;
        }
        public Rectangle GetRect(int index, bool scrollDown = true, int transformX = 0, int transformY = 0)
        {
            Rectangle returnRect = _recItemsCached[index];
            bool rTL = _owner.RightToLeft == RightToLeft.Yes;
            if (_owner.MultiColumn)
            {
                if (rTL)
                {
                    if (scrollDown)
                    {
                        return new Rectangle(returnRect.X + transformX, returnRect.Y, returnRect.Width, returnRect.Height);
                    }
                    else
                    {
                        return new Rectangle(returnRect.X - transformX, returnRect.Y, returnRect.Width, returnRect.Height);
                    }
                }
                else
                {
                    if (scrollDown)
                    {
                        return new Rectangle(returnRect.X - transformX, returnRect.Y, returnRect.Width, returnRect.Height);
                    }
                    else
                    {
                        return new Rectangle(returnRect.X + transformX, returnRect.Y, returnRect.Width, returnRect.Height);
                    }
                }
            }
            int x = returnRect.X;
            if (!rTL)
            {
                x -= transformX;
            }
            if (scrollDown)
            {
                return new Rectangle(x, returnRect.Y - transformY, returnRect.Width + transformX, returnRect.Height);
            }
            else
            {
                return new Rectangle(x, returnRect.Y + transformY, returnRect.Width + transformX, returnRect.Height);
            }

        }
    }
}
