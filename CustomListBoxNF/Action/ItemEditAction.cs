﻿using CustomListBoxNF.CustomControl;
using CustomListBoxNF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Actions
{
    internal class ItemEditorAction : UndoableAction
    {
        private CustomListBox _component { get; set; }
        private int _changedIndex;
        private object _oldData;
        private object _editorData;
        public ItemEditorAction(CustomListBox component, int changedIndex, object editData)
        {
            _component = component;
            _changedIndex = changedIndex;
            _editorData = editData;
        }

        public override bool PerformAction(object sender)
        {
            if (SaveUndoState())
            {
                _component.ChangeValueAt(_changedIndex, _editorData);
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool PerformUndo(object sender)
        {
            _component.ChangeValueAt(_changedIndex, _oldData);
            return true;
        }

        protected override bool SaveUndoState()
        {
            object data = _component.Items[_changedIndex];
            if (_component.DataSource == null || data is string || (_component.DataSource != null && _component._cachedPropertyDescs.Count == 0))
            {
                _oldData = data;
            }
            else
            {
                _oldData = Helper.GetPropertyValueFromDataSource(_component.DisplayMember, _component._cachedPropertyDescs,
                    data);
            }
            return true;
        }
    }
}
