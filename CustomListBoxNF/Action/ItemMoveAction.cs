﻿using CustomListBoxNF.CustomControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Actions
{
    internal class ItemMoveAction : UndoableAction
    {
        private CustomListBox _component { get; set; }
        private int _oldIndex, _curIndex;
        private object _dataMove;
        public ItemMoveAction(CustomListBox component, int curIndex, object dataMove)
        {
            _component = component;
            _curIndex = curIndex;
            _dataMove = dataMove;
        }

        public override bool PerformAction(object sender)
        {
            if (SaveUndoState())
            {
                _component.ChangeIndex(_oldIndex, _curIndex);
                return true;
            }
            return false;
        }

        public override bool PerformUndo(object sender)
        {
            _component.ChangeIndex(_curIndex, _oldIndex);
            return true;
        }

        protected override bool SaveUndoState()
        {
            int index = _component.Items.IndexOf(_dataMove);
            if (index <= 0 || index >= _component.Items.Count)
            {
                return false;
            }
            _oldIndex = index;
            return true;
        }
    }
}
