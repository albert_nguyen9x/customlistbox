﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Actions
{
    internal abstract class Action
    {
        public abstract bool PerformAction(object sender);
    }
    internal abstract class UndoableAction : Action
    {
        protected virtual bool IsUndoAble => true;
        protected abstract bool SaveUndoState();
        public abstract bool PerformUndo(object sender);
    }
}
