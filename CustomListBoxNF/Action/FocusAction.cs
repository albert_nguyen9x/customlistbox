﻿using CustomListBoxNF.CustomControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Actions
{
    internal class StartEditAction : Action
    {
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.StartEdit();
            }
            return false;
        }
    }
    internal class ListBoxUndoAction : Action
    {
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.Undo();
            }
            return false;
        }
    }
    internal class ListBoxRedoAction : Action
    {
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.Redo();
            }
            return false;
        }
    }
    internal abstract class NavigationAction: Action
    {
        protected bool _shiftPress;
        public NavigationAction(bool shiftPress):base()
        {
            _shiftPress = shiftPress;
        }
    }
    internal class SelectPrevColAction : NavigationAction
    {
        public SelectPrevColAction(bool shiftPress) : base(shiftPress) { }
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.SelectPrevCol(_shiftPress);
            }
            return false;
        }
    }
    internal class SelectNextColAction : NavigationAction
    {
        public SelectNextColAction(bool shiftPress) : base(shiftPress) { }
        
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.SelectNextCol(_shiftPress);
            }
            return false;
        }

    }
    internal class SelectPrevRowAction : NavigationAction
    {
        public SelectPrevRowAction(bool shiftPress) : base(shiftPress) { }
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.SelectPrevRow(_shiftPress);
            }
            return false;
        }
    }
    internal class SelectNextRowAction : NavigationAction
    {
        public SelectNextRowAction(bool shiftPress) : base(shiftPress) { }
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.SelectNextRow(_shiftPress);
            }
            return false;
        }
    }
    internal class SelectFirstPageAction : NavigationAction
    {
        public SelectFirstPageAction(bool shiftPress) : base(shiftPress) { }
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.SelectFirstPage(_shiftPress);
            }
            return false;
        }
    }
    internal class SelectLastPageAction : NavigationAction
    {
        public SelectLastPageAction(bool shiftPress) : base(shiftPress) { }
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.SelectLastPage(_shiftPress);
            }
            return false;
        }
    }
    internal class SelectFirstAction : NavigationAction
    {
        public SelectFirstAction(bool shiftPress) : base(shiftPress) { }
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.SelectFirst(_shiftPress);
            }
            return false;
        }
    }
    internal class SelectLastAction : NavigationAction
    {
        public SelectLastAction(bool shiftPress) : base(shiftPress) { }
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.SelectLast(_shiftPress);
            }
            return false;
        }
    }
}


