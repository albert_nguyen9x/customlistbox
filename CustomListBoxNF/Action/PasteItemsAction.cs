﻿using CustomListBoxNF.CustomControl;
using CustomListBoxNF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Actions
{
    internal class PasteItemsAction : UndoableAction
    {
        private CustomListBox _component;
        private int _index;
        private string[] _data;

        protected override bool IsUndoAble
        {
            get
            {
                ObjectList items = _component.Items;
                return items.Count == 0 && items[0] is string;
            }
        }
        public PasteItemsAction(CustomListBox customListBox, int index, string content)
        {
            _component = customListBox;
            _index = index;
            _data = getContent(content);
        }

        private string[] getContent(string content)
        {
            return content.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        }

        public override bool PerformAction(object sender)
        {
            if (IsUndoAble)
            {
                _component.Items.InsertRange(_index, _data);
                return true;
            }
            return false;
        }

        public override bool PerformUndo(object sender)
        {
            _component.Items.RemoveRange(_index, _data.Length);
            return true;
        }


        protected override bool SaveUndoState()
        {
            return true;
        }
    }
}
