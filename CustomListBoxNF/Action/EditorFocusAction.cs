﻿using CustomListBoxNF.CustomControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomListBoxNF.Actions
{
    internal class FinishEditAction : Action
    {
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.FinishEdit();
            }
            return false;
        }
    }
    internal class CancelEditAction : Action
    {
        public override bool PerformAction(object sender)
        {
            CustomListBox component = sender as CustomListBox;
            if (component != null)
            {
                return component.CancelEdit();
            }
            return false;
        }
    }
}
