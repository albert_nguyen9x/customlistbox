﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomListBoxNF.Forms
{
  public enum GroupType
  {
    NoFormat,
    Numeric,
    DateTime
  }
  public partial class FormatStringDialog : Form
  {
    DateTime now;
    Random r = new Random();
    public string ValueSelected { get; set; } = "";
    private List<TypeData> numericType = new List<TypeData>()
    {
      new TypeData("Numeric", "N"),
      new TypeData("Currency", "C"),
      new TypeData("Scientific", "E"),
      new TypeData("Percent", "P")
    };
    private List<TypeData> dateType = new List<TypeData>()
    {
      new TypeData("", "d"),
      new TypeData("", "D"),
      new TypeData("", "f"),
      new TypeData("", "F"),
      new TypeData("", "s"),
      new TypeData("", "G"),
      new TypeData("", "M")
    };
    public FormatStringDialog()
    {
      InitializeComponent();
      now = DateTime.Now;
      lstGroupType.DataSource = Enum.GetValues(typeof(GroupType));
      lstGroupType.SelectedIndex = 0;
      cboNumericType.DataSource = numericType;
      cboNumericType.DisplayMember = "Name";
      cboNumericType.ValueMember = "ValueFormat";
      lstDateType.DataSource = dateType;
      lstDateType.ValueMember = "ValueFormat";
      txtSample.Text = "";

      //splitContainer1.Panel1.Controls.Remove(pnDate);
      //splitContainer1.Panel1.Controls.Remove(pnNumeric);
    }

    private void lstGroupType_SelectedValueChanged(object sender, EventArgs e)
    {
      switch((GroupType)lstGroupType.SelectedItem)
      {
        case GroupType.NoFormat:
           pnDate.Visible = false;
          pnNumeric.Visible = false;
          txtSample.Text = "";
          break;
        case GroupType.Numeric:
          pnDate.Visible = false;
          pnNumeric.Visible = true;
          string format = $"{cboNumericType.SelectedValue}{numericUpDown1.Value}";
          txtSample.Text = r.NextDouble().ToString(format);
          break;
        case GroupType.DateTime:
          pnDate.Visible = true;
          pnNumeric.Visible = false;
          txtSample.Text = now.ToString(lstDateType.SelectedValue as string);
          break;
      }
    }

    private void lstDateType_DrawItem(object sender, DrawItemEventArgs e)
    {
      e.DrawBackground();
      string text = now.ToString((lstDateType.Items[e.Index] as TypeData).ValueFormat);
      e.Graphics.DrawString(text, (sender as Control).Font, Brushes.Black, e.Bounds.X, e.Bounds.Y);
    }

    private void lstDateType_SelectedValueChanged(object sender, EventArgs e)
    {
      txtSample.Text = now.ToString(lstDateType.SelectedValue as string);
    }

    private void cboNumericType_SelectedValueChanged(object sender, EventArgs e)
    {
      string format = $"{cboNumericType.SelectedValue}{numericUpDown1.Value}";
      txtSample.Text = r.NextDouble().ToString(format);
    }

    private void numericUpDown1_ValueChanged(object sender, EventArgs e)
    {
      string format = $"{cboNumericType.SelectedValue}{numericUpDown1.Value}";
      txtSample.Text = r.NextDouble().ToString(format);
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      
      string result = "";
      switch (lstGroupType.SelectedIndex)
      {
        case 1:
           result = $"{cboNumericType.SelectedValue}{numericUpDown1.Value}";
           break;
        case 2:
           result = $"{lstDateType.SelectedValue}";
           break;
      }
      ValueSelected = result;
      DialogResult = DialogResult.OK;
      Close();
    }
  }
  class TypeData
  {
    public string Name { get; set; }
    public string ValueFormat { get; set; }
    public TypeData(string _name, string _data)
    {
      Name = _name;
      ValueFormat = _data;
    }
  }
}
