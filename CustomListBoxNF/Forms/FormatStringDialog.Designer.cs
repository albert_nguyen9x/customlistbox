﻿
namespace CustomListBoxNF.Forms
{
  partial class FormatStringDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.lstGroupType = new System.Windows.Forms.ListBox();
      this.pnDate = new System.Windows.Forms.Panel();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.lstDateType = new System.Windows.Forms.ListBox();
      this.pnNumeric = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.panel2 = new System.Windows.Forms.Panel();
      this.cboNumericType = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.pnSample = new System.Windows.Forms.Panel();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.txtSample = new System.Windows.Forms.TextBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btnOK = new System.Windows.Forms.Button();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.btnCancel = new System.Windows.Forms.Button();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.pnDate.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.pnNumeric.SuspendLayout();
      this.panel3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
      this.panel2.SuspendLayout();
      this.pnSample.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.splitContainer1);
      this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox1.Location = new System.Drawing.Point(0, 0);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
      this.groupBox1.Size = new System.Drawing.Size(403, 235);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      // 
      // splitContainer1
      // 
      this.splitContainer1.Cursor = System.Windows.Forms.Cursors.VSplit;
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.Location = new System.Drawing.Point(4, 17);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.pnDate);
      this.splitContainer1.Panel2.Controls.Add(this.pnNumeric);
      this.splitContainer1.Panel2.Controls.Add(this.pnSample);
      this.splitContainer1.Panel2.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(0, 17, 0, 0);
      this.splitContainer1.Size = new System.Drawing.Size(395, 214);
      this.splitContainer1.SplitterDistance = 131;
      this.splitContainer1.SplitterWidth = 3;
      this.splitContainer1.TabIndex = 0;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.lstGroupType);
      this.groupBox2.Cursor = System.Windows.Forms.Cursors.Arrow;
      this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox2.Location = new System.Drawing.Point(0, 0);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(131, 214);
      this.groupBox2.TabIndex = 0;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Group Type:";
      // 
      // lstGroupType
      // 
      this.lstGroupType.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lstGroupType.FormattingEnabled = true;
      this.lstGroupType.Location = new System.Drawing.Point(3, 16);
      this.lstGroupType.Name = "lstGroupType";
      this.lstGroupType.Size = new System.Drawing.Size(125, 195);
      this.lstGroupType.TabIndex = 0;
      this.lstGroupType.SelectedValueChanged += new System.EventHandler(this.lstGroupType_SelectedValueChanged);
      // 
      // pnDate
      // 
      this.pnDate.Controls.Add(this.groupBox4);
      this.pnDate.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnDate.Location = new System.Drawing.Point(0, 121);
      this.pnDate.Name = "pnDate";
      this.pnDate.Size = new System.Drawing.Size(261, 93);
      this.pnDate.TabIndex = 2;
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.lstDateType);
      this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox4.Location = new System.Drawing.Point(0, 0);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(261, 93);
      this.groupBox4.TabIndex = 0;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Date type:";
      // 
      // lstDateType
      // 
      this.lstDateType.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lstDateType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.lstDateType.FormattingEnabled = true;
      this.lstDateType.ItemHeight = 15;
      this.lstDateType.Location = new System.Drawing.Point(3, 16);
      this.lstDateType.Name = "lstDateType";
      this.lstDateType.Size = new System.Drawing.Size(255, 74);
      this.lstDateType.TabIndex = 0;
      this.lstDateType.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lstDateType_DrawItem);
      this.lstDateType.SelectedValueChanged += new System.EventHandler(this.lstDateType_SelectedValueChanged);
      // 
      // pnNumeric
      // 
      this.pnNumeric.Controls.Add(this.panel3);
      this.pnNumeric.Controls.Add(this.panel2);
      this.pnNumeric.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnNumeric.Location = new System.Drawing.Point(0, 62);
      this.pnNumeric.Name = "pnNumeric";
      this.pnNumeric.Padding = new System.Windows.Forms.Padding(3);
      this.pnNumeric.Size = new System.Drawing.Size(261, 59);
      this.pnNumeric.TabIndex = 1;
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.numericUpDown1);
      this.panel3.Controls.Add(this.label2);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel3.Location = new System.Drawing.Point(3, 27);
      this.panel3.Name = "panel3";
      this.panel3.Padding = new System.Windows.Forms.Padding(2);
      this.panel3.Size = new System.Drawing.Size(255, 24);
      this.panel3.TabIndex = 1;
      // 
      // numericUpDown1
      // 
      this.numericUpDown1.Dock = System.Windows.Forms.DockStyle.Right;
      this.numericUpDown1.Location = new System.Drawing.Point(100, 2);
      this.numericUpDown1.Name = "numericUpDown1";
      this.numericUpDown1.Size = new System.Drawing.Size(153, 20);
      this.numericUpDown1.TabIndex = 3;
      this.numericUpDown1.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Left;
      this.label2.Location = new System.Drawing.Point(2, 2);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(81, 20);
      this.label2.TabIndex = 2;
      this.label2.Text = "Decimal places:";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.cboNumericType);
      this.panel2.Controls.Add(this.label1);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(3, 3);
      this.panel2.Name = "panel2";
      this.panel2.Padding = new System.Windows.Forms.Padding(2);
      this.panel2.Size = new System.Drawing.Size(255, 24);
      this.panel2.TabIndex = 0;
      // 
      // cboNumericType
      // 
      this.cboNumericType.Dock = System.Windows.Forms.DockStyle.Right;
      this.cboNumericType.FormattingEnabled = true;
      this.cboNumericType.Location = new System.Drawing.Point(99, 2);
      this.cboNumericType.Name = "cboNumericType";
      this.cboNumericType.Size = new System.Drawing.Size(154, 21);
      this.cboNumericType.TabIndex = 3;
      this.cboNumericType.SelectedValueChanged += new System.EventHandler(this.cboNumericType_SelectedValueChanged);
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(2, 2);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(93, 20);
      this.label1.TabIndex = 2;
      this.label1.Text = "Numeric type:";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pnSample
      // 
      this.pnSample.Controls.Add(this.groupBox3);
      this.pnSample.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnSample.Location = new System.Drawing.Point(0, 17);
      this.pnSample.Name = "pnSample";
      this.pnSample.Padding = new System.Windows.Forms.Padding(2);
      this.pnSample.Size = new System.Drawing.Size(261, 45);
      this.pnSample.TabIndex = 0;
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.txtSample);
      this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox3.Location = new System.Drawing.Point(2, 2);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(257, 41);
      this.groupBox3.TabIndex = 0;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Sample";
      // 
      // txtSample
      // 
      this.txtSample.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtSample.Enabled = false;
      this.txtSample.Location = new System.Drawing.Point(3, 16);
      this.txtSample.Name = "txtSample";
      this.txtSample.Size = new System.Drawing.Size(251, 20);
      this.txtSample.TabIndex = 0;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.btnOK);
      this.panel1.Controls.Add(this.splitter1);
      this.panel1.Controls.Add(this.btnCancel);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 235);
      this.panel1.Name = "panel1";
      this.panel1.Padding = new System.Windows.Forms.Padding(3);
      this.panel1.Size = new System.Drawing.Size(403, 26);
      this.panel1.TabIndex = 1;
      // 
      // btnOK
      // 
      this.btnOK.Dock = System.Windows.Forms.DockStyle.Right;
      this.btnOK.Location = new System.Drawing.Point(263, 3);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(64, 20);
      this.btnOK.TabIndex = 2;
      this.btnOK.Text = "OK";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
      this.splitter1.Location = new System.Drawing.Point(327, 3);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(9, 20);
      this.splitter1.TabIndex = 1;
      this.splitter1.TabStop = false;
      // 
      // btnCancel
      // 
      this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
      this.btnCancel.Location = new System.Drawing.Point(336, 3);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(64, 20);
      this.btnCancel.TabIndex = 0;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // FormatStringDialog
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(403, 261);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.panel1);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FormatStringDialog";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Format String Dialog";
      this.groupBox1.ResumeLayout(false);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.pnDate.ResumeLayout(false);
      this.groupBox4.ResumeLayout(false);
      this.pnNumeric.ResumeLayout(false);
      this.panel3.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
      this.panel2.ResumeLayout(false);
      this.pnSample.ResumeLayout(false);
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Panel pnNumeric;
    private System.Windows.Forms.Panel pnSample;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.ListBox lstGroupType;
    private System.Windows.Forms.Panel pnDate;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.TextBox txtSample;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.NumericUpDown numericUpDown1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.ComboBox cboNumericType;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.ListBox lstDateType;
  }
}