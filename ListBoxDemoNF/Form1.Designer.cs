﻿
namespace ListBoxDemoNF
{
  partial class Form1
  {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.button1 = new System.Windows.Forms.Button();
      this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
      this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
      this.propertyGrid2 = new System.Windows.Forms.PropertyGrid();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.customListBox1 = new CustomListBoxNF.CustomControl.CustomListBox();
      this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
      ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
      this.SuspendLayout();
      // 
      // listBox1
      // 
      this.listBox1.Items.AddRange(new object[] {
            "Item1",
            "Item2",
            "Item3",
            "Item4",
            "Item5",
            "Item6",
            "Item7",
            "Item8",
            "Item9",
            "Item10Item1Item12345"});
      this.listBox1.Location = new System.Drawing.Point(62, 12);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(120, 95);
      this.listBox1.TabIndex = 0;
      this.listBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseClick);
      this.listBox1.FormatStringChanged += new System.EventHandler(this.listBox1_FormatStringChanged);
      this.listBox1.SelectedValueChanged += new System.EventHandler(this.listBox1_SelectedValueChanged);
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(305, 142);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(64, 20);
      this.button1.TabIndex = 2;
      this.button1.Text = "Add Item";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Visible = false;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // propertyGrid2
      // 
      this.propertyGrid2.Location = new System.Drawing.Point(52, 142);
      this.propertyGrid2.Name = "propertyGrid2";
      this.propertyGrid2.SelectedObject = this.listBox1;
      this.propertyGrid2.Size = new System.Drawing.Size(214, 236);
      this.propertyGrid2.TabIndex = 6;
      // 
      // textBox1
      // 
      this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.textBox1.Location = new System.Drawing.Point(248, 43);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(100, 13);
      this.textBox1.TabIndex = 7;
      // 
      // customListBox1
      // 
      this.customListBox1.BackColor = System.Drawing.SystemColors.Window;
      this.customListBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.customListBox1.Editable = true;
      this.customListBox1.ForeColor = System.Drawing.SystemColors.WindowText;
      this.customListBox1.Items.Add("Item1");
      this.customListBox1.Items.Add("Item2");
      this.customListBox1.Items.Add("Item3");
      this.customListBox1.Items.Add("Item4");
      this.customListBox1.Items.Add("Item5");
      this.customListBox1.Items.Add("Item6");
      this.customListBox1.Items.Add("Item7");
      this.customListBox1.Items.Add("Item8");
      this.customListBox1.Items.Add("Item9");
      this.customListBox1.Items.Add("Item10Item1Item12345");
      this.customListBox1.Location = new System.Drawing.Point(442, 12);
      this.customListBox1.Name = "customListBox1";
      this.customListBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.customListBox1.Size = new System.Drawing.Size(120, 95);
      this.customListBox1.TabIndex = 5;
      this.customListBox1.SelectedIndexChanged += new System.EventHandler(this.customListBox1_SelectedIndexChanged);
      // 
      // propertyGrid1
      // 
      this.propertyGrid1.Location = new System.Drawing.Point(420, 142);
      this.propertyGrid1.Name = "propertyGrid1";
      this.propertyGrid1.SelectedObject = this.customListBox1;
      this.propertyGrid1.Size = new System.Drawing.Size(214, 236);
      this.propertyGrid1.TabIndex = 4;
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(686, 390);
      this.Controls.Add(this.textBox1);
      this.Controls.Add(this.propertyGrid2);
      this.Controls.Add(this.customListBox1);
      this.Controls.Add(this.propertyGrid1);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.listBox1);
      this.Name = "Form1";
      this.Text = "Form1";
      this.DoubleClick += new System.EventHandler(this.Form1_DoubleClick);
      ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.BindingSource bindingSource1;
    private System.ComponentModel.BackgroundWorker backgroundWorker1;
    private System.Windows.Forms.PropertyGrid propertyGrid1;
    private CustomListBoxNF.CustomControl.CustomListBox customListBox1;
    private System.Windows.Forms.PropertyGrid propertyGrid2;
    private System.Windows.Forms.TextBox textBox1;
  }
}

