﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListBoxDemoNF
{
  public partial class Form1 : Form
  {
    BindingList<Test> source = new BindingList<Test>()
      {
        new Test(){ ID = 1.001f},
        new Test(){ ID = 2.002f}
      };
    DataTable custTable = new DataTable("Customers");
    public Form1()
    {
      InitializeComponent();
      //customListBox1.AddItem("Item1");
      //customListBox1.AddItem("Item2");
      //customListBox1.AddItem("Item3");
      //customListBox1.AddItem("Item4");
      //customListBox1.AddItem("Item5");
      //customListBox1.AddItem("Item6");
      //customListBox1.AddItem("Item7");
      //customListBox1.AddItem("Item8");
      //customListBox1.AddItem("Item3");
      //customListBox1.AddItem("Item2");
      //customListBox1.AddItem("Item5");
      //customListBox1.AddItem("Item1");
      //customListBox1.AddItem("Item3");
      //customListBox1.InvalidateScroll(this, null);
      //CreateCustomersTable();
      //listBox1.DataSource = source;
      //listBox1.DisplayMember = "ID";
      //customListBox1.DataSource = source;
      //customListBox1.DisplayMember = "ID";
      //customListBox1.ValueMember = "id";
    }

    private void customListBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      //MessageBox.Show($"You select {customListBox1.SelectedItem}");
    }

    private void Form1_DoubleClick(object sender, EventArgs e)
    {
      //source.Add(new Test() { ID = source.Count + 1 }); ;
    }
    internal class Test
    {
      public float ID { get; set; }
      public override string ToString()
      {
        return $"Test{{ID : {ID} }}";
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      source.Add(new Test() { ID = source.Count + 1 });
      //listBox1.FormatString = "C2";
      //customListBox1.FormatString = "C2";
    }

    private void CreateCustomersTable()
    {
      // Create a new DataTable.    
      DataColumn dtColumn;
      DataRow myDataRow;

      // Create id column  
      dtColumn = new DataColumn();
      dtColumn.DataType = typeof(Int32);
      dtColumn.ColumnName = "id";
      dtColumn.Caption = "Cust ID";
      dtColumn.ReadOnly = false;
      dtColumn.Unique = true;
      // Add column to the DataColumnCollection.  
      custTable.Columns.Add(dtColumn);

      // Create Name column.    
      dtColumn = new DataColumn();
      dtColumn.DataType = typeof(String);
      dtColumn.ColumnName = "Name";
      dtColumn.Caption = "Cust Name";
      dtColumn.AutoIncrement = false;
      dtColumn.ReadOnly = false;
      dtColumn.Unique = false;
      /// Add column to the DataColumnCollection.  
      custTable.Columns.Add(dtColumn);

      // Create Address column.    
      dtColumn = new DataColumn();
      dtColumn.DataType = typeof(String);
      dtColumn.ColumnName = "Address";
      dtColumn.Caption = "Address";
      dtColumn.ReadOnly = false;
      dtColumn.Unique = false;
      // Add column to the DataColumnCollection.    
      custTable.Columns.Add(dtColumn);

      // Make id column the primary key column.    
      DataColumn[] PrimaryKeyColumns = new DataColumn[1];
      PrimaryKeyColumns[0] = custTable.Columns["id"];
      custTable.PrimaryKey = PrimaryKeyColumns;



      // Add data rows to the custTable using NewRow method    
      // I add three customers with their addresses, names and ids   
      myDataRow = custTable.NewRow();
      myDataRow["id"] = 1001;
      myDataRow["Address"] = "43 Lanewood Road, cito, CA";
      myDataRow["Name"] = "George Bishop";
      custTable.Rows.Add(myDataRow);
      myDataRow = custTable.NewRow();
      myDataRow["id"] = 1002;
      myDataRow["name"] = "Rock joe";
      myDataRow["Address"] = " kind of Prussia, PA";
      custTable.Rows.Add(myDataRow);
      myDataRow = custTable.NewRow();
      myDataRow["id"] = 1003;
      myDataRow["Name"] = "Miranda";
      myDataRow["Address"] = "279 P. Avenue, Bridgetown, PA";
      custTable.Rows.Add(myDataRow);
    }

    private void listBox1_SelectedValueChanged(object sender, EventArgs e)
    {

    }

    private void listBox1_FormatStringChanged(object sender, EventArgs e)
    {

    }

    private void listBox1_MouseClick(object sender, MouseEventArgs e)
    {

    }
  }
}
