﻿
namespace ListBoxDemoNF
{
  partial class Form4
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      //this.listBox1 = new System.Windows.Forms.ListBox();
      this.listBox1 = new CustomListBoxNF.CustomControl.CustomListBox();
      this.SuspendLayout();
      // 
      // listBox1
      // 
      this.listBox1.ColumnWidth = 85;
      this.listBox1.FormattingEnabled = true;
      this.listBox1.ItemHeight = 16;
      this.listBox1.Items.AddRange(new object[] {
            "Item 1, column 1",
            "Item 2, column 1",
            "Item 3, column 1",
            "Item 4, column 1",
            "Item 5, column 1",
            "Item 1, column 2",
            "Item 2, column 2",
             "Item 1, column 1",
            "Item 2, column 1",
            "Item 3, column 1",
            "Item 4, column 1",
            "Item 5, column 1",
            "Item 1, column 2",
            "Item 2, column 2",
             "Item 1, column 1",
            "Item 2, column 1",
            "Item 3, column 1",
            "Item 4, column 1",
            "Item 5, column 1",
            "Item 1, column 2",
            "Item 2, column 2",
             "Item 1, column 1",
            "Item 2, column 1",
            "Item 3, column 1",
            "Item 4, column 1",
            "Item 5, column 1",
            "Item 1, column 2",
             "Item 1, column 1",
            "Item 2, column 1",
            "Item 3, column 1",
            "Item 4, column 1",
            "Item 5, column 1",
            "Item 1, column 2",
            "Item 2, column 2",
            "Item 2, column 2",
            "Item 3, column 2"});
      this.listBox1.Location = new System.Drawing.Point(0, 0);
      this.listBox1.MultiColumn = true;
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(120, 84);
      this.listBox1.TabIndex = 0;
      // 
      // Form4
      // 
      this.ClientSize = new System.Drawing.Size(185, 126);
      this.Controls.Add(this.listBox1);
      this.Name = "Form4";
      this.ResumeLayout(false);

    }

    #endregion

    private CustomListBoxNF.CustomControl.CustomListBox listBox1;
   // private System.Windows.Forms.ListBox listBox1;
  }
}