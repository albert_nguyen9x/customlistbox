﻿
namespace ListBoxDemoNF
{
  partial class Form3
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.customListBox1 = new CustomListBoxNF.CustomControl.CustomListBox();
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.SuspendLayout();
      // 
      // customListBox1
      // 
      this.customListBox1.AllowDrop = true;
      this.customListBox1.BackColor = System.Drawing.SystemColors.Window;
      this.customListBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.customListBox1.EllipsisEnabled = true;
      this.customListBox1.ForeColor = System.Drawing.SystemColors.WindowText;
      this.customListBox1.Items.Add("asdasdassaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
      this.customListBox1.Items.Add("ads");
      this.customListBox1.Items.Add("das");
      this.customListBox1.Items.Add("das");
      this.customListBox1.Items.Add("asd");
      this.customListBox1.Items.Add("das");
      this.customListBox1.Items.Add("d");
      this.customListBox1.Items.Add("as");
      this.customListBox1.Location = new System.Drawing.Point(131, 82);
      this.customListBox1.Name = "customListBox1";
      this.customListBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.customListBox1.Size = new System.Drawing.Size(120, 94);
      this.customListBox1.TabIndex = 0;
      // 
      // listBox1
      // 
      this.listBox1.AllowDrop = true;
      this.listBox1.FormattingEnabled = true;
      this.listBox1.ItemHeight = 16;
      this.listBox1.Items.AddRange(new object[] {
            "asdasdassaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            "ads",
            "das",
            "das",
            "asd",
            "das",
            "d",
            "as"});
      this.listBox1.Location = new System.Drawing.Point(380, 82);
      this.listBox1.Name = "listBox1";
      this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.listBox1.Size = new System.Drawing.Size(120, 84);
      this.listBox1.TabIndex = 1;
      // 
      // Form3
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(800, 450);
      this.Controls.Add(this.listBox1);
      this.Controls.Add(this.customListBox1);
      this.Name = "Form3";
      this.Text = "Form3";
      this.ResumeLayout(false);

    }

    #endregion

    private CustomListBoxNF.CustomControl.CustomListBox customListBox1;
    private System.Windows.Forms.ListBox listBox1;
  }
}